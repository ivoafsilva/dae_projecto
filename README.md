# **Web Store** #

![](http://i.imgur.com/DkYUJnp.png)

### What is this repository? ###

* This was a private repository for a school project, now made public for review purposes.
* Web store fully writting in **Java EE** using **Prime Faces**.

### How do I get set up? ###

* Just clone the repo, and create a new database named "LOJA_VIRTUAL" with user and password "sa".