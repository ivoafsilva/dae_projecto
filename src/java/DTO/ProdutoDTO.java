/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author ptumas
 */
public class ProdutoDTO {
    private String referencia;
    private String nome;
    private String descricao;
    private String descricaoLonga;
    private byte[] imagem;
    private float preco;
    private int idCategoria;

    public ProdutoDTO() {
    }

    public ProdutoDTO(String referencia, String nome, String descricao, String descricaoLonga, byte[] imagem, float preco, int idCategoria) {
        this.referencia = referencia;
        this.nome = nome;
        this.descricao = descricao;
        this.descricaoLonga = descricaoLonga;
        this.imagem = imagem;
        this.preco = preco;
        this.idCategoria = idCategoria;
    }
    
    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public byte[] getImagem() {
        return imagem;
    }

    public void setImagem(byte[] imagem) {
        this.imagem = imagem;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    /**
     * @return the descricaoLonga
     */
    public String getDescricaoLonga() {
        return descricaoLonga;
    }

    /**
     * @param descricaoLonga the descricaoLonga to set
     */
    public void setDescricaoLonga(String descricaoLonga) {
        this.descricaoLonga = descricaoLonga;
    }
    
    
}
