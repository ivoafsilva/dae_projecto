/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.Date;
import utils.Estado;

/**
 *
 * @author ptumas
 */
public class EncomendaCabecalhoConsumidorDTO extends EncomendaCabecalhoDTO {

    private String emailRequerente;
    private String nome;
    private String apelido;
    private String morada;
    private String localidade;

    public EncomendaCabecalhoConsumidorDTO() {
    }
    
    public EncomendaCabecalhoConsumidorDTO(String emailRequerente, String nomeRequerente, String codigoPostal, String morada, String localidade, String idEncomenda, Estado estado, int idLojaExpedicao, String nomeLojaExpedicao, Date dataEncomenda) {
        super(idEncomenda, estado, idLojaExpedicao, nomeLojaExpedicao, dataEncomenda, codigoPostal);
        this.emailRequerente = emailRequerente;
        String[] nomeCompleto = nomeRequerente.split(" ");
        this.nome = nomeCompleto[0];
        this.apelido = nomeCompleto[1];
        this.morada = morada;
        this.localidade = localidade;
    }

    public EncomendaCabecalhoConsumidorDTO(String emailRequerente, String nomeRequerente, String codigoPostal, String morada, String localidade, String idEncomenda, Estado estado, int idLojaExpedicao, String nomeLojaExpedicao, String codigoPostalLojaExpedicao, Date dataEncomenda, Date dataExpedicao, String emailFuncionarioExpedicao) {
        super(idEncomenda, estado, idLojaExpedicao, nomeLojaExpedicao, dataEncomenda, codigoPostal, dataExpedicao, emailFuncionarioExpedicao);
        this.emailRequerente = emailRequerente;
        String[] nomeCompleto = nomeRequerente.split(" ");
        this.nome = nomeCompleto[0];
        this.apelido = nomeCompleto[1];
        this.morada = morada;
        this.localidade = localidade;
    }

    public String getEmailRequerente() {
        return emailRequerente;
    }

    public void setEmailRequerente(String emailRequerente) {
        this.emailRequerente = emailRequerente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public String getMorada() {
        return morada;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    public String getLocalidade() {
        return localidade;
    }

    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }
}
