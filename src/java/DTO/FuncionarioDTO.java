/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author ptumas
 */
public class FuncionarioDTO {
    
    private String nome;
    private String email;
    private int idLoja;

    public FuncionarioDTO() {
    }

    public FuncionarioDTO(String nome, String email, int idLoja) {
        this.nome = nome;
        this.email = email;
        this.idLoja = idLoja;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIdLoja() {
        return idLoja;
    }

    public void setIdLoja(int idLoja) {
        this.idLoja = idLoja;
    }
    
    
}
