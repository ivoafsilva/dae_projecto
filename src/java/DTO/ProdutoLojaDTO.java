/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author ptumas
 */
public class ProdutoLojaDTO {
    
    private String referenciaProduto;
    private int idLoja;
    private String numeroSerie;

    public ProdutoLojaDTO() {
    }

    public ProdutoLojaDTO(String referenciaProduto, int idLoja, String numeroSerie) {
        this.referenciaProduto = referenciaProduto;
        this.idLoja = idLoja;
        this.numeroSerie = numeroSerie;
    }
    
    public String getReferenciaProduto() {
        return referenciaProduto;
    }

    public void setReferenciaProduto(String referenciaProduto) {
        this.referenciaProduto = referenciaProduto;
    }

    public int getIdLoja() {
        return idLoja;
    }

    public void setIdLoja(int idLoja) {
        this.idLoja = idLoja;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }
    
    
}
