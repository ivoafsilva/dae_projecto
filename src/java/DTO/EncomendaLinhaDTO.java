/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.List;

/**
 *
 * @author ptumas
 */
public class EncomendaLinhaDTO {

    private ProdutoDTO produto;
    private List<String> numerosSerie;
    private int quantidade;

    public EncomendaLinhaDTO(ProdutoDTO produto, int quantidade) {
        this.produto = produto;
        this.quantidade = quantidade;
    }

    public ProdutoDTO getProduto() {
        return produto;
    }

    public void setProduto(ProdutoDTO produto) {
        this.produto = produto;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public float getPrecoTotal() {
        return quantidade * produto.getPreco();
    }

    public List<String> getNumerosSerie() {
        return numerosSerie;
    }

    public void setNumerosSerie(List<String> numeroSerie) {
        this.numerosSerie = numeroSerie;
    }

    public void addNumeroSerie(String numeroSerie) {
        this.numerosSerie.add(numeroSerie);
    }

    public void removeNumeroSerie(String numeroSerie) {
        this.numerosSerie.remove(numeroSerie);
    }
}
