/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import utils.Estado;

public abstract class EncomendaCabecalhoDTO {

    private String idEncomenda;
    private Estado estado;
    private int idLojaExpedicao;
    private String nomeLojaExpedicao;
    private Date dataEncomenda;
    private String codigoPostal;
    private Date dataExpedicao;
    private List<EncomendaLinhaDTO> linhasEncomenda;
    private String emailFuncionarioExpedicao;

    public EncomendaCabecalhoDTO() {
    }

    
    
    public EncomendaCabecalhoDTO(String id, Estado estado, int idLojaExpedicao, String nomeLojaExpedicao, Date dataEncomenda, String codigoPostal) {
        this.idEncomenda = id;
        this.estado = estado;
        this.idLojaExpedicao = idLojaExpedicao;
        this.nomeLojaExpedicao = nomeLojaExpedicao;
        this.dataEncomenda = dataEncomenda;
        this.codigoPostal = codigoPostal;
        this.linhasEncomenda = new ArrayList<>();
    }

    public EncomendaCabecalhoDTO(String id, Estado estado, int idLojaExpedicao, String nomeLojaExpedicao, Date dataEncomenda, String codigoPostal, Date dataExpedicao, String emailFuncionarioExpedicao) {
        this.idEncomenda = id;
        this.estado = estado;
        this.idLojaExpedicao = idLojaExpedicao;
        this.nomeLojaExpedicao = nomeLojaExpedicao;
        this.dataEncomenda = dataEncomenda;
        this.codigoPostal = codigoPostal;
        this.dataExpedicao = dataExpedicao;
        this.emailFuncionarioExpedicao = emailFuncionarioExpedicao;
    }

    public String getIdEncomenda() {
        return idEncomenda;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public int getIdLojaExpedicao() {
        return idLojaExpedicao;
    }

    public void setIdLojaExpedicao(int idLojaExpedicao) {
        this.idLojaExpedicao = idLojaExpedicao;
    }

    public String getNomeLojaExpedicao() {
        return nomeLojaExpedicao;
    }

    public void setNomeLojaExpedicao(String nomeLojaExpedicao) {
        this.nomeLojaExpedicao = nomeLojaExpedicao;
    }

    public Date getDataEncomenda() {
        return dataEncomenda;
    }

    public void setDataEncomenda(Date dataEncomenda) {
        this.dataEncomenda = dataEncomenda;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public Date getDataExpedicao() {
        return dataExpedicao;
    }

    public void setDataExpedicao(Date dataExpedicao) {
        this.dataExpedicao = dataExpedicao;
    }

    public String getEmailFuncionarioExpedicao() {
        return emailFuncionarioExpedicao;
    }

    public void setEmailFuncionarioExpedicao(String emailFuncionarioExpedicao) {
        this.emailFuncionarioExpedicao = emailFuncionarioExpedicao;
    }

    public void addLinhaEncomenda(EncomendaLinhaDTO encomendaLinha) {
        this.linhasEncomenda.add(encomendaLinha);
    }

    public void removeLinhaEncomenda(EncomendaLinhaDTO encomendaLinha) {
        this.linhasEncomenda.remove(encomendaLinha);
    }

    public void setLinhasEncomenda(List<EncomendaLinhaDTO> encomendasLinha) {
        this.linhasEncomenda = encomendasLinha;
    }
}
