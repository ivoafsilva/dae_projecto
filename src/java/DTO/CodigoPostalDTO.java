/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author ptumas
 */
public class CodigoPostalDTO {
    
    private String codigoPostal;
    private String localidade;

    public CodigoPostalDTO() {
    }

    public CodigoPostalDTO(String codigoPostal, String localidade) {
        this.codigoPostal = codigoPostal;
        this.localidade = localidade;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getLocalidade() {
        return localidade;
    }

    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }
    
    
}
