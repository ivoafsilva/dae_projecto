/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Unksafi
 */
public class EncomendaDTO {

    private EncomendaCabecalhoDTO cabecalho;
    private List<EncomendaLinhaDTO> linhas;

    public EncomendaDTO() {
        this.cabecalho = null;
        this.linhas = new ArrayList<>();
    }

    public EncomendaDTO(EncomendaCabecalhoDTO cabecalho) {
        this.cabecalho = cabecalho;
        this.linhas = new ArrayList<>();
    }

    public EncomendaCabecalhoDTO getCabecalho() {
        return cabecalho;
    }

    public void setCabecalho(EncomendaCabecalhoDTO cabecalho) {
        this.cabecalho = cabecalho;
    }

    public List<EncomendaLinhaDTO> getLinhas() {
        return linhas;
    }

    public void setLinhas(List<EncomendaLinhaDTO> linhas) {
        this.linhas = linhas;
    }

    public void addLinhaEncomenda(EncomendaLinhaDTO linha) {
        this.linhas.add(linha);
    }

    public void removeLinhaEncomenda(EncomendaLinhaDTO linha) {
        this.linhas.remove(linha);
    }

    public int getQuantidadeTotal() {

        int quantidadeTotal = 0;

        for (EncomendaLinhaDTO encomendaLinhaDTO : linhas) {
            quantidadeTotal += encomendaLinhaDTO.getQuantidade();
        }

        return quantidadeTotal;
    }

    public float getPrecoTotal() {

        float precoTotal = 0;

        for (EncomendaLinhaDTO encomendaLinhaDTO : linhas) {
            precoTotal += encomendaLinhaDTO.getPrecoTotal();
        }

        return precoTotal;

    }
}
