/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.Date;
import java.util.List;
import utils.Estado;

public class EncomendaCabecalhoFuncionarioDTO extends EncomendaCabecalhoDTO {

    /* Quem encomenda é o funcionario, não a loja.
    private int idLojaAEncomendar;
    private String nomeLojaAEncomendar;
    * */
    private String emailFuncionarioAEncomendar;

    public EncomendaCabecalhoFuncionarioDTO() {
    }
    
    public EncomendaCabecalhoFuncionarioDTO(String emailFuncionarioAEncomendar, String idEncomenda, Estado estado, int idLojaExpedicao, String nomeLojaExpedicao, Date dataEncomenda, String codigoPostal) {
        super(idEncomenda, estado, idLojaExpedicao, nomeLojaExpedicao, dataEncomenda, codigoPostal);
        this.emailFuncionarioAEncomendar = emailFuncionarioAEncomendar;
    }

    public EncomendaCabecalhoFuncionarioDTO(String emailFuncionarioAEncomendar, String idEncomenda, Estado estado, int idLojaExpedicao, String nomeLojaExpedicao, Date dataEncomenda, String codigoPostal, Date dataExpedicao, List<EncomendaLinhaDTO> linhasEncomenda, String emailFuncionarioExpedicao) {
        super(idEncomenda, estado, idLojaExpedicao, nomeLojaExpedicao, dataEncomenda, codigoPostal, dataExpedicao, emailFuncionarioExpedicao);
        this.emailFuncionarioAEncomendar = emailFuncionarioAEncomendar;
    }

    public String getEmailFuncionarioAEncomendar() {
        return emailFuncionarioAEncomendar;
    }

    public void setEmailFuncionarioAEncomendar(String emailFuncionarioAEncomendar) {
        this.emailFuncionarioAEncomendar = emailFuncionarioAEncomendar;
    }
}
