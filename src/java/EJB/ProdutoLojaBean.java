/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import DTO.ProdutoLojaDTO;
import Entidades.Loja;
import Entidades.Produto;
import Entidades.ProdutoLoja;
import excepcoes.EntidadeNaoExistenteException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Unksafi
 */
@Stateless
public class ProdutoLojaBean {

    @PersistenceContext
    private EntityManager em;

    public void adicionarProdutoALoja(int id_loja, String ref_produto, String numeroSerie) {
        try {
            Loja loja = (Loja) em.find(Loja.class, id_loja);
            Produto produto = (Produto) em.find(Produto.class, ref_produto);

            if (loja != null && produto != null) {
                ProdutoLoja produtoLoja = new ProdutoLoja(produto, loja, numeroSerie);
                loja.addProdutosLoja(produtoLoja);
                produto.addProdutosLoja(produtoLoja);
                em.persist(produtoLoja);
            }

        } catch (Exception Ex) {
            throw new EJBException(Ex.getMessage());
        }
    }

    public void removerProdutoALoja(int id_loja, String ref_produto, String numeroSerie) {
        try {
            Loja loja = (Loja) em.find(Loja.class, id_loja);
            Produto produto = (Produto) em.find(Produto.class, ref_produto);
            ProdutoLoja produtoLojaARemover = new ProdutoLoja(produto, loja, numeroSerie);

            ProdutoLoja produtoLojaDaBD = (ProdutoLoja) em.find(ProdutoLoja.class, produtoLojaARemover);

            if (loja != null && produto != null && produtoLojaDaBD != null) {

                loja.removeProdutosLoja(produtoLojaDaBD);
                produto.removeProdutosLoja(produtoLojaDaBD);
                em.remove(produtoLojaDaBD);
            }

        } catch (Exception Ex) {
            throw new EJBException(Ex.getMessage());
        }
    }

    public int getStockProdutoPorLojaNaoReservado(int id_loja, String ref_produto) {
        try {
            Loja loja = (Loja) em.find(Loja.class, id_loja);
            if (loja == null) {
                throw new EntidadeNaoExistenteException("Loja não existente!");
            }

            Produto produto = (Produto) em.find(Produto.class, ref_produto);
            if (produto == null) {
                throw new EntidadeNaoExistenteException("Produto não existente!");
            }

            long stock = (long) em.createNamedQuery("countNumerosSerieNaoReservados").setParameter("referenciaProduto", ref_produto).setParameter("idLoja", id_loja).getSingleResult();
            return (int)stock;
        } catch (EntidadeNaoExistenteException e) {
            throw new EJBException(e.getMessage());
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public int getStockProdutoPorLojaReservado(int id_loja, String ref_produto) {
        try {
            Loja loja = (Loja) em.find(Loja.class, id_loja);
            if (loja == null) {
                throw new EntidadeNaoExistenteException("Loja não existente!");
            }

            Produto produto = (Produto) em.find(Produto.class, ref_produto);
            if (produto == null) {
                throw new EntidadeNaoExistenteException("Produto não existente!");
            }

            int stock = ((Integer) em.createNamedQuery("countNumerosSerieReservados").setParameter("referenciaProduto", ref_produto).setParameter("idLoja", id_loja).getSingleResult()).intValue();
            return stock;
        } catch (EntidadeNaoExistenteException e) {
            throw new EJBException(e.getMessage());
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public List<String> getNumerosSerieDeProdutoPorLojaReservados(int id_loja, String ref_produto) {
        List<String> numerosSerie = new ArrayList<>();
        try {
            Loja loja = (Loja) em.find(Loja.class, id_loja);
            if (loja == null) {
                throw new EntidadeNaoExistenteException("Loja não existente!");
            }

            Produto produto = (Produto) em.find(Produto.class, ref_produto);
            if (produto == null) {
                throw new EntidadeNaoExistenteException("Produto não existente!");
            }

            List<ProdutoLoja> produtosLoja = (List<ProdutoLoja>) em.createNamedQuery("findNumerosSerieReservados").setParameter("referenciaProduto", ref_produto).setParameter("idLoja", id_loja).getResultList();
            for (ProdutoLoja produtoLoja : produtosLoja) {
                numerosSerie.add(produtoLoja.getNumeroSerie());
            }
            return numerosSerie;

        } catch (EntidadeNaoExistenteException e) {
            throw new EJBException(e.getMessage());
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public List<String> getNumerosSerieDeProdutoPorLojaNaoReservados(int id_loja, String ref_produto) {
        List<String> numerosSerie = new ArrayList<>();
        try {
            Loja loja = (Loja) em.find(Loja.class, id_loja);
            if (loja == null) {
                throw new EntidadeNaoExistenteException("Loja não existente!");
            }

            Produto produto = (Produto) em.find(Produto.class, ref_produto);
            if (produto == null) {
                throw new EntidadeNaoExistenteException("Produto não existente!");
            }

            List<ProdutoLoja> produtosLoja = (List<ProdutoLoja>) em.createNamedQuery("findNumerosSerieNaoReservados").setParameter("referenciaProduto", ref_produto).setParameter("idLoja", id_loja).getResultList();
            for (ProdutoLoja produtoLoja : produtosLoja) {
                numerosSerie.add(produtoLoja.getNumeroSerie());
            }
            return numerosSerie;

        } catch (EntidadeNaoExistenteException e) {
            throw new EJBException(e.getMessage());
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public ProdutoLojaDTO getPrimeiroNumeroSerieDeProdutoPorLojaNaoReservado(int id_loja, String ref_produto) {
        try {
            Loja loja = (Loja) em.find(Loja.class, id_loja);
            if (loja == null) {
                throw new EntidadeNaoExistenteException("Loja não existente!");
            }

            Produto produto = (Produto) em.find(Produto.class, ref_produto);
            if (produto == null) {
                throw new EntidadeNaoExistenteException("Produto não existente!");
            }

            ProdutoLoja produtoLoja = (ProdutoLoja) em.createNamedQuery("findNumerosSerieNaoReservados").setParameter("referenciaProduto", ref_produto).setParameter("idLoja", id_loja).setMaxResults(1).getSingleResult();

            return new ProdutoLojaDTO(produtoLoja.getProduto().getReferencia(), produtoLoja.getLoja().getIdLoja(), produtoLoja.getNumeroSerie());

        } catch (EntidadeNaoExistenteException e) {
            throw new EJBException(e.getMessage());
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
}
