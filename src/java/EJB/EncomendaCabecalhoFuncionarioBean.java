/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import DTO.EncomendaCabecalhoFuncionarioDTO;
import DTO.EncomendaLinhaDTO;
import Entidades.CodigoPostal;
import Entidades.EncomendaCabecalho;
import Entidades.EncomendaCabecalhoFuncionario;
import Entidades.EncomendaLinha;
import utils.Estado;
import Entidades.Funcionario;
import Entidades.Loja;
import excepcoes.EntidadeNaoExistenteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class EncomendaCabecalhoFuncionarioBean {

    @EJB
    ProdutoLojaBean produtoLojaBean;
    @PersistenceContext
    private EntityManager em;

    public String criarEncomendaCabecalhoFuncionario(Estado estado, Date data, String idCodigoPostal, int idLoja, String emailFuncionario) {
        try {

            CodigoPostal codigoPostal = (CodigoPostal) em.find(CodigoPostal.class, idCodigoPostal);
            if (codigoPostal == null) {
                throw new EntidadeNaoExistenteException("CódigoPostal não existente!");
            }

            Loja loja = (Loja) em.find(Loja.class, idLoja);
            if (loja == null) {
                throw new EntidadeNaoExistenteException("Loja não existente!");
            }

            Funcionario funcionario = (Funcionario) em.find(Funcionario.class, emailFuncionario);
            if (funcionario == null) {
                throw new EntidadeNaoExistenteException("Funcionário não existente!");
            }

            EncomendaCabecalho encomendaCabecalho = new EncomendaCabecalhoFuncionario(estado, loja, data, codigoPostal, funcionario);

            codigoPostal.addEncomenda(encomendaCabecalho);
            loja.addEncomenda(encomendaCabecalho);
            funcionario.addEncomenda(encomendaCabecalho);

            em.persist(encomendaCabecalho);
            return encomendaCabecalho.getIdEncomenda();
        } catch (Exception Ex) {
            throw new EJBException(Ex.getMessage());
        }
    }

    public List<EncomendaCabecalhoFuncionarioDTO> getAllEncomendasFuncionario() {
        try {
            List<EncomendaCabecalhoFuncionario> encomendas = (List<EncomendaCabecalhoFuncionario>) em.createNamedQuery("findAllEncomendasFuncionario").getResultList();
            return copiarEncomendaFuncionarioParaDTOs(encomendas);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public List<EncomendaCabecalhoFuncionarioDTO> getAllEncomendasDespachadasFuncionario() {
        try {
            List<EncomendaCabecalhoFuncionario> encomendas = (List<EncomendaCabecalhoFuncionario>) em.createNamedQuery("findEncomendasDespachadasFuncionario").getResultList();
            return copiarEncomendaFuncionarioParaDTOs(encomendas);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public List<EncomendaCabecalhoFuncionarioDTO> getAllEncomendasNaoDespachadasFuncionario() {
        try {
            List<EncomendaCabecalhoFuncionario> encomendas = (List<EncomendaCabecalhoFuncionario>) em.createNamedQuery("findEncomendasNaoDespachadasFuncionario").getResultList();
            return copiarEncomendaFuncionarioParaDTOs(encomendas);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public List<EncomendaCabecalhoFuncionarioDTO> getAllEncomendasAguardarStockFuncionario() {
        try {
            List<EncomendaCabecalhoFuncionario> encomendas = (List<EncomendaCabecalhoFuncionario>) em.createNamedQuery("findEncomendasAguardarStockFuncionario").getResultList();
            return copiarEncomendaFuncionarioParaDTOs(encomendas);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public List<EncomendaCabecalhoFuncionarioDTO> getAllEncomendasFuncionarioPorLoja(int idLoja) {
        try {
            List<EncomendaCabecalhoFuncionario> encomendas = (List<EncomendaCabecalhoFuncionario>) em.createNamedQuery("findEncomendasFuncionarioPorLoja").setParameter("idLoja", idLoja).getResultList();
            return copiarEncomendaFuncionarioParaDTOs(encomendas);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public boolean despacharEncomendaFuncionario(EncomendaCabecalhoFuncionario encomenda, Funcionario funcionario) {


        for (EncomendaLinha encomendaLinha : encomenda.getLinhasEncomenda()) {
            //encontrar o produtoLoja correspondente e retirar o produto
            produtoLojaBean.removerProdutoALoja(encomenda.getLojaExpedicao().getIdLoja(), encomendaLinha.getProduto().getReferencia(), encomendaLinha.getNumeroSerie());
        }

        encomenda.setDataExpedicao(new Date());
        encomenda.setFuncionarioExpedicao(funcionario);
        encomenda.setEstado(Estado.DESPACHADA);
        encomenda.setLojaExpedicao(funcionario.getLoja());

        em.persist(encomenda);
        em.flush();

        return true;
    }

    public Boolean alterarEncomendaFuncionario(EncomendaCabecalhoFuncionarioDTO encomendaNova) {
        try {
            EncomendaCabecalhoFuncionario encomendaAntiga = (EncomendaCabecalhoFuncionario) em.find(EncomendaCabecalhoFuncionario.class, encomendaNova.getIdEncomenda());
            if (encomendaAntiga == null) {
                throw new EntidadeNaoExistenteException("Encomenda não existe!");
            }

            CodigoPostal codigoPostalNovo = (CodigoPostal) em.find(CodigoPostal.class, encomendaNova.getCodigoPostal());
            if (codigoPostalNovo == null) {
                throw new EntidadeNaoExistenteException("Código Postal não existente!");
            }

            Loja lojaNova = (Loja) em.find(Loja.class, encomendaNova.getIdLojaExpedicao());
            if (lojaNova == null) {
                throw new EntidadeNaoExistenteException("Loja não existente!");
            }

            Funcionario funcionarioExpedicaoNovo = null;
            if (encomendaNova.getEmailFuncionarioExpedicao() != null) {
                funcionarioExpedicaoNovo = (Funcionario) em.find(Funcionario.class, encomendaNova.getEmailFuncionarioExpedicao());
                if (funcionarioExpedicaoNovo == null) {
                    throw new EntidadeNaoExistenteException("Funcionario não existente!");
                }
            }

            encomendaAntiga.setDataEncomenda(encomendaNova.getDataEncomenda());
            encomendaAntiga.setDataExpedicao(encomendaNova.getDataExpedicao());
            encomendaAntiga.setEstado(encomendaNova.getEstado());

            if (codigoPostalNovo != null && encomendaAntiga.getCodigoPostal() != codigoPostalNovo) {
                encomendaAntiga.getCodigoPostal().removeEncomenda(encomendaAntiga);
                encomendaAntiga.setCodigoPostal(codigoPostalNovo);
                encomendaAntiga.getCodigoPostal().addEncomenda(encomendaAntiga);
            }

            if (lojaNova != null && encomendaAntiga.getLojaExpedicao() != lojaNova) {
                encomendaAntiga.getLojaExpedicao().removeEncomenda(encomendaAntiga);
                encomendaAntiga.setLojaExpedicao(lojaNova);
                encomendaAntiga.getLojaExpedicao().addEncomenda(encomendaAntiga);
            }

            if (funcionarioExpedicaoNovo != null && encomendaAntiga.getFuncionarioExpedicao() != funcionarioExpedicaoNovo) {
                encomendaAntiga.getFuncionarioExpedicao().removeEncomenda(encomendaAntiga);
                encomendaAntiga.setFuncionarioExpedicao(funcionarioExpedicaoNovo);
                encomendaAntiga.getFuncionarioExpedicao().addEncomenda(encomendaAntiga);
            }

            em.persist(encomendaAntiga);
            em.flush();

        } catch (EntidadeNaoExistenteException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    private List<EncomendaCabecalhoFuncionarioDTO> copiarEncomendaFuncionarioParaDTOs(List<EncomendaCabecalhoFuncionario> encomendas) {
        List<EncomendaCabecalhoFuncionarioDTO> dtos = new ArrayList<>();

        for (EncomendaCabecalhoFuncionario encomenda : encomendas) {
            dtos.add(copiarEncomendaFuncionarioParaDTO(encomenda));
        }
        return dtos;
    }

    private EncomendaCabecalhoFuncionarioDTO copiarEncomendaFuncionarioParaDTO(EncomendaCabecalhoFuncionario encomenda) {
        List<EncomendaLinhaDTO> encomendasLinhasDTO = new ArrayList<>();

        for (EncomendaLinha encomendaLinha : encomenda.getLinhasEncomenda()) {
            //FIXME
            //EncomendaLinhaDTO encomendaLinhaDTO = new EncomendaLinhaDTO(encomendaLinha);
            //encomendasLinhasDTO.add(encomendaLinhaDTO);
        }

        EncomendaCabecalhoFuncionarioDTO encomendaDTO = new EncomendaCabecalhoFuncionarioDTO(
                encomenda.getFuncionarioAEncomendar().getEmail(),
                encomenda.getIdEncomenda(),
                encomenda.getEstado(),
                encomenda.getLojaExpedicao().getIdLoja(),
                encomenda.getLojaExpedicao().getNome(),
                encomenda.getDataEncomenda(),
                encomenda.getCodigoPostal().getCodigo_postal());

        if (encomenda.getDataExpedicao() != null) {
            encomendaDTO.setDataExpedicao(encomenda.getDataExpedicao());
        }

        if (encomenda.getFuncionarioExpedicao() != null) {
            encomendaDTO.setEmailFuncionarioExpedicao(encomenda.getFuncionarioExpedicao().getEmail());
        }

        if (!(encomendasLinhasDTO.isEmpty())) {
            encomendaDTO.setLinhasEncomenda(encomendasLinhasDTO);
        }
        return encomendaDTO;
    }
}
