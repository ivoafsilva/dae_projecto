/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import utils.Estado;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
public class ConfigBean {

    @EJB
    CodigoPostalBean codigoPostalBean;
    @EJB
    ProdutoBean produtoBean;
    @EJB
    LojaBean lojaBean;
    @EJB
    FuncionarioBean funcionarioBean;
    @EJB
    ProdutoLojaBean produtoLojaBean;
    @EJB
    EncomendaCabecalhoFuncionarioBean encomendaCabecalhoFuncionarioBean;
    @EJB
    EncomendaCabecalhoConsumidorBean encomendaCabecalhoConsumidorBean;
    @EJB
    EncomendaLinhaBean encomendaLinhaBean;
    @EJB
    CategoriaBean categoriaBean;

    @PostConstruct
    public void populateBD() {

        byte[] imagem = null;

        try {
            lojaBean.criarLoja(1, "Loja de Lisboa");
            lojaBean.criarLoja(2, "Loja do Porto");
            lojaBean.criarLoja(3, "Loja de Coimbra");
            lojaBean.criarLoja(4, "Loja de Faro");
            lojaBean.criarLoja(5, "Loja do Funchal");
            lojaBean.criarLoja(6, "Loja de Leiria");
            lojaBean.criarLoja(7, "Loja de Ponta Delgada");
            lojaBean.criarLoja(8, "Loja de Vila Real");
            lojaBean.criarLoja(9, "Loja de Viseu");



            //TODOS OS CÓDIGOS POSTAIS PRINCIPAIS
            //O ALGORÍTMO DE PROCURA IRÁ PROCURAR PELOS 4 DÍGITOS E SE
            //NÃO ENCONTRAR, PROCURAR POR 3, 2 OU MESMO 1 DÍGITO
            codigoPostalBean.CriarCodigoPostal("1000-000", "Lisboa", 1);
            codigoPostalBean.CriarCodigoPostal("1050-000", "Lisboa", 1);
            codigoPostalBean.CriarCodigoPostal("1070-000", "Lisboa", 1);
            codigoPostalBean.CriarCodigoPostal("1100-000", "Lisboa", 1);
            codigoPostalBean.CriarCodigoPostal("1150-000", "Lisboa", 1);
            codigoPostalBean.CriarCodigoPostal("1170-000", "Lisboa", 1);
            codigoPostalBean.CriarCodigoPostal("1200-000", "Lisboa", 1);
            codigoPostalBean.CriarCodigoPostal("1250-000", "Lisboa", 1);
            codigoPostalBean.CriarCodigoPostal("1300-000", "Lisboa", 1);
            codigoPostalBean.CriarCodigoPostal("1350-000", "Lisboa", 1);
            codigoPostalBean.CriarCodigoPostal("1400-000", "Lisboa", 1);
            codigoPostalBean.CriarCodigoPostal("1500-000", "Lisboa", 1);
            codigoPostalBean.CriarCodigoPostal("1600-000", "Lisboa", 1);
            codigoPostalBean.CriarCodigoPostal("1700-000", "Lisboa", 1);
            codigoPostalBean.CriarCodigoPostal("1750-000", "Lisboa", 1);
            codigoPostalBean.CriarCodigoPostal("1800-000", "Lisboa", 1);
            codigoPostalBean.CriarCodigoPostal("1900-000", "Lisboa", 1);
            codigoPostalBean.CriarCodigoPostal("1990-000", "Lisboa", 1);
            codigoPostalBean.CriarCodigoPostal("4000-000", "Porto", 2);
            codigoPostalBean.CriarCodigoPostal("4050-000", "Porto", 2);
            codigoPostalBean.CriarCodigoPostal("4100-000", "Porto", 2);
            codigoPostalBean.CriarCodigoPostal("4150-000", "Porto", 2);
            codigoPostalBean.CriarCodigoPostal("4200-000", "Porto", 2);
            codigoPostalBean.CriarCodigoPostal("4250-000", "Porto", 2);
            codigoPostalBean.CriarCodigoPostal("4300-000", "Porto", 2);
            codigoPostalBean.CriarCodigoPostal("4350-000", "Porto", 2);
            codigoPostalBean.CriarCodigoPostal("3800-000", "Aveiro", 3);
            codigoPostalBean.CriarCodigoPostal("3810-000", "Aveiro", 3);
            codigoPostalBean.CriarCodigoPostal("7800-000", "Beja", 4);
            codigoPostalBean.CriarCodigoPostal("4700-000", "Braga", 2);
            codigoPostalBean.CriarCodigoPostal("4705-000", "Braga", 2);
            codigoPostalBean.CriarCodigoPostal("4710-000", "Braga", 2);
            codigoPostalBean.CriarCodigoPostal("4715-000", "Braga", 2);
            codigoPostalBean.CriarCodigoPostal("5300-000", "Bragança", 8);
            codigoPostalBean.CriarCodigoPostal("6000-000", "Castelo Branco", 9);
            codigoPostalBean.CriarCodigoPostal("3000-000", "Coimbra", 3);
            codigoPostalBean.CriarCodigoPostal("3020-000", "Coimbra", 3);
            codigoPostalBean.CriarCodigoPostal("3025-000", "Coimbra", 3);
            codigoPostalBean.CriarCodigoPostal("3030-000", "Coimbra", 3);
            codigoPostalBean.CriarCodigoPostal("3040-000", "Coimbra", 3);
            codigoPostalBean.CriarCodigoPostal("3045-000", "Coimbra", 3);
            codigoPostalBean.CriarCodigoPostal("7000-000", "Évora", 4);
            codigoPostalBean.CriarCodigoPostal("7005-000", "Évora", 4);
            codigoPostalBean.CriarCodigoPostal("8000-000", "Faro", 4);
            codigoPostalBean.CriarCodigoPostal("8805-000", "Faro", 4);
            codigoPostalBean.CriarCodigoPostal("9000-000", "Funchal", 5);
            codigoPostalBean.CriarCodigoPostal("9020-000", "Funchal", 5);
            codigoPostalBean.CriarCodigoPostal("9030-000", "Funchal", 5);
            codigoPostalBean.CriarCodigoPostal("9050-000", "Funchal", 5);
            codigoPostalBean.CriarCodigoPostal("6300-000", "Guarda", 9);
            codigoPostalBean.CriarCodigoPostal("2400-000", "Leiria", 6);
            codigoPostalBean.CriarCodigoPostal("2405-000", "Leiria", 6);
            codigoPostalBean.CriarCodigoPostal("2410-000", "Leiria", 6);
            codigoPostalBean.CriarCodigoPostal("2415-000", "Leiria", 6);
            codigoPostalBean.CriarCodigoPostal("2420-000", "Leiria", 6);
            codigoPostalBean.CriarCodigoPostal("9500-000", "Ponta Delgada", 7);
            codigoPostalBean.CriarCodigoPostal("7300-000", "Portalegre", 4);
            codigoPostalBean.CriarCodigoPostal("2000-000", "Santarém", 6);
            codigoPostalBean.CriarCodigoPostal("2005-000", "Santarém", 6);
            codigoPostalBean.CriarCodigoPostal("2900-000", "Setúbal", 1);
            codigoPostalBean.CriarCodigoPostal("2910-000", "Setúbal", 1);
            codigoPostalBean.CriarCodigoPostal("4900-000", "Viana do Castelo", 2);
            codigoPostalBean.CriarCodigoPostal("5000-000", "Vila Real", 8);
            codigoPostalBean.CriarCodigoPostal("3500-000", "Viseu", 9);
            codigoPostalBean.CriarCodigoPostal("3505-000", "Viseu", 9);
            codigoPostalBean.CriarCodigoPostal("3510-000", "Viseu", 9);
            codigoPostalBean.CriarCodigoPostal("3515-000", "Viseu", 9);

            //CÓDIGOS POSTAIS CRIADOS DE PROPÓSITO PARA FAZER OS VÁRIOS TESTES
            codigoPostalBean.CriarCodigoPostal("2410-457", "Leiria", 6);
            codigoPostalBean.CriarCodigoPostal("2405-002", "Maceira", 6);
            codigoPostalBean.CriarCodigoPostal("3000-010", "Coimbra", 3);
            codigoPostalBean.CriarCodigoPostal("3820-010", "Ílhavo", 3);

            funcionarioBean.criarFuncionario("Antonio Funcionario", "antonio@funcionario.com", "funcionario", 1);
            funcionarioBean.criarFuncionario("Jervasio Funcionario", "jervasio@funcionario.com", "funcionario", 1);
            funcionarioBean.criarFuncionario("Quim ze Funcionario", "quim_ze@funcionario.com", "funcionario", 1);
            funcionarioBean.criarFuncionario("Joaquim Funcionario", "joaquim@funcionario.com", "funcionario", 2);
            funcionarioBean.criarFuncionario("Alfredo Funcionario", "alfredo@funcionario.com", "funcionario", 2);
            funcionarioBean.criarFuncionario("Pedro Funcionario", "pedro@funcionario.com", "funcionario", 3);

            categoriaBean.CriarCategoria(1, "Tablets");
            categoriaBean.CriarCategoria(2, "Desktops");
            categoriaBean.CriarCategoria(3, "Armazenamento");

            imagem = ImageToByte(new File(getClass().getResource("/resources/Samsung_Galaxy_Tab_3.jpg").getFile()));
            produtoBean.criarProduto("SGTVJ57H82", "Samsung Galaxy Tab 3",
                    "Tablet Samsung Galaxy 3</br>Android",
                    "1.2 GHz Tablet Processor</br>"
                    + "1 GB DDR2 SDRAM</br>"
                    + "8 GB Serial ATA</br>"
                    + "Android 4.1 Jelly Bean", imagem, (float) 169, 1);

            imagem = ImageToByte(new File(getClass().getResource("/resources/Google_Nexus_7_Tablet.jpg").getFile()));
            produtoBean.criarProduto("GNT5J793FV", "Google Nexus 7",
                    "Tablet Google Nexus 7</br>Android",
                    "1.5 GHz Snapdragon S4</br>"
                    + "2 GB DDR3 SDRAM</br>"
                    + "32 GB Serial ATA</br>"
                    + "Android 3.0 (Honeycomb)", imagem, (float) 267.61, 1);

            imagem = ImageToByte(new File(getClass().getResource("/resources/Google_Nexus_10.jpg").getFile()));
            produtoBean.criarProduto("GN10FK79V2", "Google Nexus 10",
                    "Tablet Google Nexus 10</br>Android",
                    "1.5 GHz Snapdragon S4</br>"
                    + "2 GB DDR2 SDRAM</br>"
                    + "16 GB Serial ATA</br>"
                    + "Android 4.3 (Jelly Bean)", imagem, (float) 424.99, 1);

            imagem = ImageToByte(new File(getClass().getResource("/resources/ASUS_MeMOPad_HD.jpg").getFile()));
            produtoBean.criarProduto("ASUSMM71HD", "ASUS MeMOPad HD",
                    "Tablet ASUS MeMOPad</br>Android",
                    "1.2 GHz Intel Celeron</br>"
                    + "1 GB DDR2 SDRAM</br>"
                    + "16 GB Serial ATA</br>"
                    + "Android 4.1 (Jelly Bean)", imagem, (float) 297, 1);

            imagem = ImageToByte(new File(getClass().getResource("/resources/CyberpowerPC_Gamer_Ultra.jpg").getFile()));
            produtoBean.criarProduto("PCGUA880XA", "CyberpowerPC Gamer Ultra",
                    "CyberpowerPC</br>PC de alto desempenho",
                    "3.8 GHz FX-Series Quad-Core FX-4100</br>"
                    + "8 GB DDR3 SDRAM</br>"
                    + "1 TB Serial ATA-600</br>"
                    + "Windows 7 Home Premium 64-bit", imagem, (float) 995, 2);

            imagem = ImageToByte(new File(getClass().getResource("/resources/Dell_Inspiron_660s.jpg").getFile()));
            produtoBean.criarProduto("DELL2314BK", "Dell Inspiron 660s",
                    "PC Dell Inspiron 660s</br>PC de baixo custo",
                    "3.4 GHz Intel Core i3-3240</br>"
                    + "4 GB</br>"
                    + "1 TB</br>"
                    + "Windows 7 Home Premium", imagem, (float) 579, 2);

            imagem = ImageToByte(new File(getClass().getResource("/resources/Lenovo_0958B2U.jpg").getFile()));
            produtoBean.criarProduto("LENOVO0695", "Lenovo ThinkCentre 0958B2U",
                    "PC Lenovo ThinkCentre</br>PC de custo médio",
                    "3.2 GHz Core i5-3470T</br>"
                    + "4 GB DDR3 SDRAM</br>"
                    + "500 GB Serial ATA</br>"
                    + "Windows 7 Professional", imagem, (float) 528.39, 2);

            imagem = ImageToByte(new File(getClass().getResource("/resources/Lenovo_3493GPU.jpg").getFile()));
            produtoBean.criarProduto("LENOVO3494", "Lenovo ThinkCentre 3493GPU",
                    "PC Lenovo ThinkCentre</br>PC de baixo custo",
                    "3.3 GHz Core i3-3217U</br>"
                    + "4 GB SDRAM</br>"
                    + "500 GB Serial ATA</br>"
                    + "Windows 7 Professional 64", imagem, (float) 473, 2);

            imagem = ImageToByte(new File(getClass().getResource("/resources/WD_Blue_1TB_WD10.jpg").getFile()));
            produtoBean.criarProduto("WD10EZEX45", "WD Blue 1 TB",
                    "Disco Rígido Externo WD</br>Capacidade: 1TB",
                    "3.5 Inch</br>"
                    + "7200 RPM</br>"
                    + "SATA 6 Gb/s</br>"
                    + "64 MB Cache - </br>"
                    + "1000 GB Serial ATA-600</br>", imagem, (float) 59.99, 3);

            imagem = ImageToByte(new File(getClass().getResource("/resources/WD_My_Passport_Ultra_1TB.jpg").getFile()));
            produtoBean.criarProduto("WDBZFP0010", "WD My Passport Ultra 1TB",
                    "Disco Rígido Externo WD Passport</br>Capacidade: 1TB",
                    "Portable External Hard Drive</br>"
                    + "USB 3.0 with Auto and Cloud Backup</br>"
                    + "1 TB", imagem, (float) 66.82, 3);

            imagem = ImageToByte(new File(getClass().getResource("/resources/Toshiba_HDTB210.jpg").getFile()));
            produtoBean.criarProduto("HDTB210XK3", "Toshiba Canvio 1TB",
                    "Disco Rígido Externo Toshiba</br>Capacidade: 1TB",
                    "USB 3.0 Basics Portable Hard Drive - (Black)</br>"
                    + "1024 GB</br>", imagem, (float) 70.65, 3);
            imagem = ImageToByte(new File(getClass().getResource("/resources/Samsung_Electronics_840.jpg").getFile()));
            produtoBean.criarProduto("7TE250BW45", "Samsung Electronics 840",
                    "Disco Rígido SSD Samsung</br>Capacidade: 250GB",
                    "250 GB</br>"
                    + "2.5-Inch</br>"
                    + "SATA III Single Unit Version Internal Solid State Drive", imagem, (float) 160.99, 3);

            produtoLojaBean.adicionarProdutoALoja(1, "SGTVJ57H82", "11111111");
            produtoLojaBean.adicionarProdutoALoja(1, "GNT5J793FV", "11111112");
            produtoLojaBean.adicionarProdutoALoja(1, "DELL2314BK", "11111113");
            produtoLojaBean.adicionarProdutoALoja(1, "LENOVO3494", "11111114");
            produtoLojaBean.adicionarProdutoALoja(1, "WDBZFP0010", "11111115");
            produtoLojaBean.adicionarProdutoALoja(1, "7TE250BW45", "11111116");

            produtoLojaBean.adicionarProdutoALoja(2, "ASUSMM71HD", "22222222");
            produtoLojaBean.adicionarProdutoALoja(2, "GN10FK79V2", "22222223");
            produtoLojaBean.adicionarProdutoALoja(2, "PCGUA880XA", "22222224");
            produtoLojaBean.adicionarProdutoALoja(2, "LENOVO0695", "22222225");
            produtoLojaBean.adicionarProdutoALoja(2, "HDTB210XK3", "22222226");
            produtoLojaBean.adicionarProdutoALoja(2, "WD10EZEX45", "22222227");

            produtoLojaBean.adicionarProdutoALoja(3, "GN10FK79V2", "33333333");
            produtoLojaBean.adicionarProdutoALoja(3, "ASUSMM71HD", "33333334");
            produtoLojaBean.adicionarProdutoALoja(3, "DELL2314BK", "33333335");
            produtoLojaBean.adicionarProdutoALoja(3, "LENOVO3494", "33333336");
            produtoLojaBean.adicionarProdutoALoja(3, "WDBZFP0010", "33333337");
            produtoLojaBean.adicionarProdutoALoja(3, "HDTB210XK3", "33333338");


            String idEncomendaConsumidor1 = encomendaCabecalhoConsumidorBean.criarEncomendaCabecalhoConsumidor(Estado.NAO_DESPACHADA, new Date(),
                    "Ivo1 Consumidor", "ivo@consumidor.org", "2410-457", "Rua dos Ofícios Nº 14", 1);
            String idEncomendaConsumidor2 = encomendaCabecalhoConsumidorBean.criarEncomendaCabecalhoConsumidor(Estado.DESPACHADA, new Date(),
                    "Ivo2 Consumidor", "ivo@consumidor.org", "2410-457", "Rua dos Ofícios Nº 14", 1);
            String idEncomendaConsumidor3 = encomendaCabecalhoConsumidorBean.criarEncomendaCabecalhoConsumidor(Estado.DESPACHADA, new Date(),
                    "Ivo3 Consumidor", "ivo@consumidor.org", "2410-457", "Rua dos Ofícios Nº 14", 2);
            String idEncomendaConsumidor4 = encomendaCabecalhoConsumidorBean.criarEncomendaCabecalhoConsumidor(Estado.NAO_DESPACHADA, new Date(),
                    "Ivo4 Consumidor", "ivo@consumidor.org", "2410-457", "Rua dos Ofícios Nº 14", 2);
            String idEncomendaConsumidor5 = encomendaCabecalhoConsumidorBean.criarEncomendaCabecalhoConsumidor(Estado.AGUARDA_STOCK, new Date(),
                    "Ivo5 Consumidor", "ivo@consumidor.org", "2410-457", "Rua dos Ofícios Nº 14", 3);

            String idEncomendaFuncionario1 = encomendaCabecalhoFuncionarioBean.criarEncomendaCabecalhoFuncionario(Estado.AGUARDA_STOCK, new Date(),
                    "4000-000", 1, "pedro@funcionario.com");
            String idEncomendaFuncionario2 = encomendaCabecalhoFuncionarioBean.criarEncomendaCabecalhoFuncionario(Estado.NAO_DESPACHADA, new Date(),
                    "3000-010", 2, "antonio@funcionario.com");
            String idEncomendaFuncionario3 = encomendaCabecalhoFuncionarioBean.criarEncomendaCabecalhoFuncionario(Estado.DESPACHADA, new Date(),
                    "3000-010", 2, "antonio@funcionario.com");
            String idEncomendaFuncionario4 = encomendaCabecalhoFuncionarioBean.criarEncomendaCabecalhoFuncionario(Estado.NAO_DESPACHADA, new Date(),
                    "3000-000", 3, "joaquim@funcionario.com");
            String idEncomendaFuncionario5 = encomendaCabecalhoFuncionarioBean.criarEncomendaCabecalhoFuncionario(Estado.DESPACHADA, new Date(),
                    "3000-000", 3, "joaquim@funcionario.com");


            encomendaLinhaBean.adicionarLinhaAEncomenda("GNT5J793FV", "11111112", idEncomendaConsumidor1);
            encomendaLinhaBean.adicionarLinhaAEncomenda("LENOVO0695", "22222225", idEncomendaConsumidor1);

            encomendaLinhaBean.adicionarLinhaAEncomenda("GN10FK79V2", "22222223", idEncomendaConsumidor2);
            encomendaLinhaBean.adicionarLinhaAEncomenda("SGTVJ57H82", "11111111", idEncomendaConsumidor2);

            encomendaLinhaBean.adicionarLinhaAEncomenda("DELL2314BK", "33333335", idEncomendaConsumidor3);
            encomendaLinhaBean.adicionarLinhaAEncomenda("ASUSMM71HD", "33333334", idEncomendaConsumidor3);

            encomendaLinhaBean.adicionarLinhaAEncomenda("WDBZFP0010", "33333337", idEncomendaConsumidor4);
            encomendaLinhaBean.adicionarLinhaAEncomenda("LENOVO3494", "11111114", idEncomendaConsumidor4);

            encomendaLinhaBean.adicionarLinhaAEncomenda("HDTB210XK3", "22222226", idEncomendaConsumidor5);
            encomendaLinhaBean.adicionarLinhaAEncomenda("HDTB210XK3", "33333338", idEncomendaConsumidor5);


            encomendaLinhaBean.adicionarLinhaAEncomenda("LENOVO3494", "11111114", idEncomendaFuncionario1);
            encomendaLinhaBean.adicionarLinhaAEncomenda("HDTB210XK3", "33333338", idEncomendaFuncionario2);
            encomendaLinhaBean.adicionarLinhaAEncomenda("DELL2314BK", "33333335", idEncomendaFuncionario3);
            encomendaLinhaBean.adicionarLinhaAEncomenda("GN10FK79V2", "22222223", idEncomendaFuncionario4);
            encomendaLinhaBean.adicionarLinhaAEncomenda("GNT5J793FV", "11111112", idEncomendaFuncionario5);

        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public static byte[] ImageToByte(File file) throws FileNotFoundException {
        FileInputStream fis = new FileInputStream(file);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        try {
            for (int readNum; (readNum = fis.read(buf)) != -1;) {
                bos.write(buf, 0, readNum);
                //System.out.println("read " + readNum + " bytes,");
            }
        } catch (IOException ex) {
        }
        byte[] bytes = bos.toByteArray();

        return bytes;
    }
}
