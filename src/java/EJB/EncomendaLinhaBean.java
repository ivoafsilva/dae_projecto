/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import Entidades.EncomendaCabecalho;
import Entidades.EncomendaLinha;
import Entidades.Produto;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Unksafi
 */
@Stateless
public class EncomendaLinhaBean {

    @PersistenceContext
    private EntityManager em;

    public void adicionarLinhaAEncomenda(String ref_produto, String numeroSerie, String id_encomendaCabecalho) {
        try {
            EncomendaCabecalho encomendaCabecalho = (EncomendaCabecalho) em.find(EncomendaCabecalho.class, id_encomendaCabecalho);
            Produto produto = (Produto) em.find(Produto.class, ref_produto);

            if (produto != null && encomendaCabecalho != null) {
                EncomendaLinha encomendaLinha = new EncomendaLinha(produto, numeroSerie, encomendaCabecalho);

                produto.addLinhaEncomendas(encomendaLinha);
                encomendaCabecalho.addLinhaEncomenda(encomendaLinha);

                em.persist(encomendaLinha);
            }

        } catch (Exception Ex) {
            throw new EJBException(Ex.getMessage());
        }
    }
}
