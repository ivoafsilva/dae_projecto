/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import DTO.FuncionarioDTO;
import DTO.ProdutoDTO;
import Entidades.Funcionario;
import Entidades.Loja;
import Entidades.Produto;
import excepcoes.EntidadeNaoExistenteException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class FuncionarioBean {

    @PersistenceContext
    private EntityManager em;

    public void criarFuncionario(String nome, String email, String password, int idLoja) {
        try {
            Loja loja = (Loja) em.find(Loja.class, idLoja);
            if (loja == null) {
                throw new EntidadeNaoExistenteException("Loja não existente!");
            }

            Funcionario funcionario = new Funcionario(nome, email, password.toCharArray(), loja);
            loja.addFuncionario(funcionario);
            em.persist(funcionario);
        } catch (Exception Ex) {
            throw new EJBException(Ex.getMessage());
        }
    }
    
    
    public List<FuncionarioDTO> getAllFuncionarios() {
        try {
            List<Funcionario> funcionarios = (List<Funcionario>) em.createNamedQuery("findAllFuncionarios").getResultList();
            return copiarFuncionariosParaDTOs(funcionarios);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
    
    
    private List<FuncionarioDTO> copiarFuncionariosParaDTOs(List<Funcionario> funcionarios) {
        List<FuncionarioDTO> dtos = new ArrayList<>();

        for (Funcionario funcionario : funcionarios) {
            dtos.add(copiarFuncionarioParaDTO(funcionario));
        }
        return dtos;
    }

    private FuncionarioDTO copiarFuncionarioParaDTO(Funcionario funcionario) {
        return new FuncionarioDTO(funcionario.getNome(), funcionario.getEmail(), funcionario.getLoja().getIdLoja());
    }
}
