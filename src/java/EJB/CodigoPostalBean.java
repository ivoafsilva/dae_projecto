/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import Entidades.CodigoPostal;
import Entidades.Loja;
import excepcoes.EntidadeNaoExistenteException;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author ptumas
 */
@Stateless
public class CodigoPostalBean {

    @PersistenceContext
    private EntityManager em;

    public void CriarCodigoPostal(String codigoPostal, String localidade, int idLoja) {

        try {
            Loja loja = (Loja) em.find(Loja.class, idLoja);
            if (loja == null) {
                throw new EntidadeNaoExistenteException("Loja não existente!");
            }
            CodigoPostal codigoPostalNovo = new CodigoPostal(codigoPostal, localidade, loja);

            loja.addCodigoPostal(codigoPostalNovo);

            em.persist(codigoPostalNovo);
            em.flush();
        } catch (Exception Ex) {
            throw new EJBException(Ex.getMessage());
        }
    }
    
    public String findCodigoPostal(String codigoPostal){
        String codigoEncontrado = null;
        try {
            codigoEncontrado = (String) em.createNamedQuery("findCodigoPostal").setParameter("codigoPostal", codigoPostal + "%").setMaxResults(1).getSingleResult();   
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
        return codigoEncontrado;
    }
}
