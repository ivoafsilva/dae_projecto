/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import DTO.ProdutoDTO;
import Entidades.Categoria;
import Entidades.Produto;
import excepcoes.EntidadeExistenteException;
import excepcoes.EntidadeNaoExistenteException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class ProdutoBean {

    @PersistenceContext
    private EntityManager em;

    public Boolean criarProduto(String referencia, String nome, String descricao, String descricaoLonga, byte[] imagem, float preco, int idCategoria) {
        try {

            Categoria categoria = (Categoria) em.find(Categoria.class, idCategoria);
            if (categoria == null) {
                throw new EntidadeNaoExistenteException("Categoria não existente!");
            }

            Produto produto = (Produto) em.find(Produto.class, referencia);
            if (produto != null) {
                throw new EntidadeExistenteException("Produto já existe!");

            }

            Produto produtoNovo = new Produto(referencia, nome, descricao, descricaoLonga, imagem, preco, categoria);
            categoria.addProduto(produtoNovo);
            em.persist(produtoNovo);
            return true;
        } catch (Exception Ex) {
            throw new EJBException(Ex.getMessage());
        }
    }

    public Boolean criarProduto(ProdutoDTO produto) {
        return criarProduto(produto.getReferencia(), produto.getNome(), produto.getDescricao(), produto.getDescricaoLonga(),
                produto.getImagem(), produto.getPreco(), produto.getIdCategoria());
    }

    public List<ProdutoDTO> getAllProdutos() {
        try {
            List<Produto> produtos = (List<Produto>) em.createNamedQuery("findAllProdutos").getResultList();
            return copiarProdutosParaDTOs(produtos);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public List<ProdutoDTO> getProdutosPorCategoria(int idCategoria) throws EntidadeNaoExistenteException {
        try {
            Categoria cat = em.find(Categoria.class, idCategoria);
            if (cat == null) {
                throw new EntidadeNaoExistenteException("Categoria não existente");
            }

            List<Produto> produtos = (List<Produto>) cat.getProdutos();
            if (produtos == null) {
                System.out.println("Produtos null!!!");
            } else {
                if (produtos.isEmpty()) {
                    System.out.println("Produtos vazios!!!");
                } else {
                    System.out.println("Número de Produtos:" + produtos.size());
                }
            }
            return copiarProdutosParaDTOs(produtos);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public ProdutoDTO getProduto(String referenciaProduto) {
        try {
            Produto produto = em.find(Produto.class, referenciaProduto);
            if (produto == null) {
                System.out.println("getProduto - Produto=null");
            }
            return copiarProdutoParaDTO(produto);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    private List<ProdutoDTO> copiarProdutosParaDTOs(List<Produto> produtos) {
        List<ProdutoDTO> dtos = new ArrayList<>();

        for (Produto produto : produtos) {
            dtos.add(copiarProdutoParaDTO(produto));
        }
        return dtos;
    }

    private ProdutoDTO copiarProdutoParaDTO(Produto produto) {
        return new ProdutoDTO(
                produto.getReferencia(),
                produto.getNome(),
                produto.getDescricao(),
                produto.getDescricaoLonga(),
                produto.getImagem(),
                produto.getPreco(),
                produto.getCategoria().getId());
    }

    public Boolean alterarProduto(ProdutoDTO produtoNovo) {
        try {
            Produto produtoAntigo = (Produto) em.find(Produto.class, produtoNovo.getReferencia());
            if (produtoAntigo == null) {
                throw new EntidadeNaoExistenteException("Produto não existe!");
            }


            Categoria novaCategoria = (Categoria) em.find(Categoria.class, produtoNovo.getIdCategoria());
            if (novaCategoria == null) {
                throw new EntidadeNaoExistenteException("Categoria não existente!");
            }

            produtoAntigo.setDescricao(produtoNovo.getDescricao());
            produtoAntigo.setPreco(produtoNovo.getPreco());
            produtoAntigo.setNome(produtoNovo.getNome());
            produtoAntigo.setDescricaoLonga(produtoNovo.getDescricaoLonga());
            if (produtoNovo.getImagem() != null) {
                produtoAntigo.setImagem(produtoNovo.getImagem());
            }

            if (produtoAntigo.getCategoria() != novaCategoria) {
                produtoAntigo.getCategoria().removeProduto(produtoAntigo);
                produtoAntigo.setCategoria(novaCategoria);
                produtoAntigo.getCategoria().addProduto(produtoAntigo);
            }

            em.persist(produtoAntigo);
            em.flush();

        } catch (EntidadeNaoExistenteException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }
}
