/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import DTO.EncomendaCabecalhoConsumidorDTO;
import DTO.EncomendaLinhaDTO;
import DTO.ProdutoDTO;
import Entidades.CodigoPostal;
import Entidades.EncomendaCabecalhoConsumidor;
import Entidades.EncomendaLinha;
import Entidades.Funcionario;
import utils.Estado;
import Entidades.Loja;
import excepcoes.EntidadeNaoExistenteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Unksafi
 */
@Stateless
public class EncomendaCabecalhoConsumidorBean {

    @EJB
    ProdutoLojaBean produtoLojaBean;
    @PersistenceContext
    private EntityManager em;
    @EJB
    EmailBean emailBean;

    public String criarEncomendaCabecalhoConsumidor(Estado estado, Date data, String nomeRequente, String emailRequerente, String idCodigoPostal, String moradaRequerente, int id_loja) {
        try {

            Loja loja = (Loja) em.find(Loja.class, id_loja);
            if (loja == null) {
                throw new EntidadeNaoExistenteException("Loja não existente!");
            }

            CodigoPostal codigoPostal = (CodigoPostal) em.find(CodigoPostal.class, idCodigoPostal);
            if (codigoPostal == null) {
                throw new EntidadeNaoExistenteException("Código Postal não existente!");
            }

            EncomendaCabecalhoConsumidor encomendaCabecalhoConsumidor = new EncomendaCabecalhoConsumidor(estado, loja, data, nomeRequente, emailRequerente, codigoPostal, moradaRequerente);

            loja.addEncomenda(encomendaCabecalhoConsumidor);
            codigoPostal.addEncomenda(encomendaCabecalhoConsumidor);

            em.persist(encomendaCabecalhoConsumidor);
            return encomendaCabecalhoConsumidor.getIdEncomenda();
        } catch (Exception Ex) {
            throw new EJBException(Ex.getMessage());
        }

    }

    public List<EncomendaCabecalhoConsumidorDTO> getAllEncomendasConsumidor() {
        try {
            List<EncomendaCabecalhoConsumidor> encomendas = (List<EncomendaCabecalhoConsumidor>) em.createNamedQuery("findAllEncomendasConsumidor").getResultList();
            return copiarEncomendaConsumidorParaDTOs(encomendas);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public List<EncomendaCabecalhoConsumidorDTO> getAllEncomendasConsumidorPorEstado(int id_loja, Estado estado) {
        try {
            List<EncomendaCabecalhoConsumidor> encomendasResultantes = new ArrayList<>();
            List<EncomendaCabecalhoConsumidor> encomendas = (List<EncomendaCabecalhoConsumidor>) em.createNamedQuery("findAllEncomendasConsumidor").getResultList();
            for (EncomendaCabecalhoConsumidor encomendaCabecalhoConsumidor : encomendas) {
                if(encomendaCabecalhoConsumidor.getEstado() == estado && encomendaCabecalhoConsumidor.getLojaExpedicao().getIdLoja() == id_loja)
                    encomendasResultantes.add(encomendaCabecalhoConsumidor);
            }
            
            return copiarEncomendaConsumidorParaDTOs(encomendasResultantes);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public Boolean alterarEncomendaConsumidor(EncomendaCabecalhoConsumidorDTO encomendaNova) {
        try {
            EncomendaCabecalhoConsumidor encomendaAntiga = (EncomendaCabecalhoConsumidor) em.find(EncomendaCabecalhoConsumidor.class, encomendaNova.getIdEncomenda());
            if (encomendaAntiga == null) {
                throw new EntidadeNaoExistenteException("Encomenda não existe!");
            }


            CodigoPostal codigoPostalNovo = (CodigoPostal) em.find(CodigoPostal.class, encomendaNova.getCodigoPostal());
            if (codigoPostalNovo == null) {
                throw new EntidadeNaoExistenteException("Código Postal não existente!");
            }

            Loja lojaNova = (Loja) em.find(Loja.class, encomendaNova.getIdLojaExpedicao());
            if (lojaNova == null) {
                throw new EntidadeNaoExistenteException("Loja não existente!");
            }

            Funcionario funcionarioExpedicaoNovo = null;
            if (encomendaNova.getEmailFuncionarioExpedicao() != null) {
                funcionarioExpedicaoNovo = (Funcionario) em.find(Funcionario.class, encomendaNova.getEmailFuncionarioExpedicao());
                if (funcionarioExpedicaoNovo == null) {
                    throw new EntidadeNaoExistenteException("Funcionario não existente!");
                }
            }

            encomendaAntiga.setDataEncomenda(encomendaNova.getDataEncomenda());
            encomendaAntiga.setDataExpedicao(encomendaNova.getDataExpedicao());
            encomendaAntiga.setEmailRequerente(encomendaNova.getEmailRequerente());
            encomendaAntiga.setEstado(encomendaNova.getEstado());
            encomendaAntiga.setMorada(encomendaNova.getMorada());
            encomendaAntiga.setNomeRequerente(encomendaNova.getNome() + " " + encomendaNova.getApelido());

            if (codigoPostalNovo != null && encomendaAntiga.getCodigoPostal() != codigoPostalNovo) {
                encomendaAntiga.getCodigoPostal().removeEncomenda(encomendaAntiga);
                encomendaAntiga.setCodigoPostal(codigoPostalNovo);
                encomendaAntiga.getCodigoPostal().addEncomenda(encomendaAntiga);
            }

            if (lojaNova != null && encomendaAntiga.getLojaExpedicao() != lojaNova) {
                encomendaAntiga.getLojaExpedicao().removeEncomenda(encomendaAntiga);
                encomendaAntiga.setLojaExpedicao(lojaNova);
                encomendaAntiga.getLojaExpedicao().addEncomenda(encomendaAntiga);
            }

            if (funcionarioExpedicaoNovo != null && encomendaAntiga.getFuncionarioExpedicao() != funcionarioExpedicaoNovo) {
                encomendaAntiga.getFuncionarioExpedicao().removeEncomenda(encomendaAntiga);
                encomendaAntiga.setFuncionarioExpedicao(funcionarioExpedicaoNovo);
                encomendaAntiga.getFuncionarioExpedicao().addEncomenda(encomendaAntiga);
            }

            em.persist(encomendaAntiga);
            em.flush();

        } catch (EntidadeNaoExistenteException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    private List<EncomendaCabecalhoConsumidorDTO> copiarEncomendaConsumidorParaDTOs(List<EncomendaCabecalhoConsumidor> encomendas) {
        List<EncomendaCabecalhoConsumidorDTO> dtos = new ArrayList<>();

        for (EncomendaCabecalhoConsumidor encomenda : encomendas) {
            dtos.add(copiarEncomendaConsumidorParaDTO(encomenda));
        }
        return dtos;
    }

    private EncomendaCabecalhoConsumidorDTO copiarEncomendaConsumidorParaDTO(EncomendaCabecalhoConsumidor encomenda) {

        EncomendaCabecalhoConsumidorDTO encomendaDTO = new EncomendaCabecalhoConsumidorDTO(
                encomenda.getEmailRequerente(),
                encomenda.getNomeRequerente(),
                encomenda.getCodigoPostal().getCodigo_postal(),
                encomenda.getMorada(),
                encomenda.getCodigoPostal().getLocalidade(),
                encomenda.getIdEncomenda(),
                encomenda.getEstado(),
                encomenda.getLojaExpedicao().getIdLoja(),
                encomenda.getLojaExpedicao().getNome(),
                encomenda.getDataEncomenda());

        if (encomenda.getDataExpedicao() != null) {
            encomendaDTO.setDataExpedicao(encomenda.getDataExpedicao());
        }

        if (encomenda.getFuncionarioExpedicao() != null) {
            encomendaDTO.setEmailFuncionarioExpedicao(encomenda.getFuncionarioExpedicao().getEmail());
        }
        return encomendaDTO;
    }
}
