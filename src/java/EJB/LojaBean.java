/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import DTO.LojaDTO;
import Entidades.CodigoPostal;
import Entidades.Loja;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class LojaBean {

    @EJB
    CodigoPostalBean codigoPostalBean;
    @PersistenceContext
    private EntityManager em;

    public void criarLoja(int id_loja, String nome) {
        try {
            Loja loja = new Loja(id_loja, nome);
            em.persist(loja);

        } catch (Exception Ex) {
            throw new EJBException(Ex.getMessage());
        }
    }

    public LojaDTO findLojaPorCodigoPostal(String id_codigoPostal) {

        try {
            CodigoPostal codigoPostal = (CodigoPostal) em.find(CodigoPostal.class, id_codigoPostal);

            if (codigoPostal != null) {
                return new LojaDTO(codigoPostal.getLoja().getIdLoja(), codigoPostal.getLoja().getNome());
            } else {
                
                String codigoPostalEncontrado = null;
                while ((!id_codigoPostal.equals("")) || codigoPostalEncontrado != null) {
                    codigoPostalEncontrado = codigoPostalBean.findCodigoPostal(id_codigoPostal);
                    id_codigoPostal = id_codigoPostal.substring(0, id_codigoPostal.length() - 1);
                }

                if (codigoPostalEncontrado != null) {
                    codigoPostal = (CodigoPostal) em.find(CodigoPostal.class, codigoPostalEncontrado);
                    return new LojaDTO(codigoPostal.getLoja().getIdLoja(), codigoPostal.getLoja().getNome());
                } else {
                    Loja loja = (Loja) em.find(Loja.class, 1);
                    return new LojaDTO(loja.getIdLoja(), loja.getNome());
                }
            }
        } catch (Exception ex) {
            throw new EJBException(ex.getMessage());
        }
    }
}
