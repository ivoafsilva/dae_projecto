/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import DTO.EncomendaCabecalhoConsumidorDTO;
import DTO.EncomendaDTO;
import DTO.EncomendaLinhaDTO;
import DTO.LojaDTO;
import DTO.ProdutoLojaDTO;
import Entidades.CodigoPostal;
import Entidades.EncomendaCabecalhoConsumidor;
import Entidades.EncomendaLinha;
import Entidades.Loja;
import Entidades.Produto;
import Entidades.ProdutoLoja;
import Entidades.ProdutoLojaKey;
import excepcoes.EntidadeNaoExistenteException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import utils.Estado;

/**
 *
 * @author Unksafi
 */
@Stateless
public class EncomendaBean {

    @EJB
    LojaBean lojaBean;
    @EJB
    ProdutoLojaBean produtoLojaBean;
    @EJB
    CodigoPostalBean codigoPostalBean;
    @EJB
    EmailBean emailBean;
    @PersistenceContext
    private EntityManager em;

    public void gravarEncomenda(EncomendaDTO encomenda) {

        int numProdutosEmStockNaLoja = 0;
        List<EncomendaLinha> linhasEncomendaFinal = new ArrayList<>();

        LojaDTO lojaDTO = lojaBean.findLojaPorCodigoPostal(encomenda.getCabecalho().getCodigoPostal());
        Loja loja = (Loja) em.find(Loja.class, lojaDTO.getId());

        encomenda.getCabecalho().setEstado(Estado.NAO_DESPACHADA);

        try {

            for (EncomendaLinhaDTO linha : encomenda.getLinhas()) {
                numProdutosEmStockNaLoja = produtoLojaBean.getStockProdutoPorLojaNaoReservado(lojaDTO.getId(), linha.getProduto().getReferencia());

                for (int i = 0; i < linha.getQuantidade(); i++) {

                    Produto produto = (Produto) em.find(Produto.class, linha.getProduto().getReferencia());

                    if (numProdutosEmStockNaLoja > 0) {
                        ProdutoLojaDTO produtoLojaDTO = produtoLojaBean.getPrimeiroNumeroSerieDeProdutoPorLojaNaoReservado(lojaDTO.getId(), linha.getProduto().getReferencia());

                        ProdutoLojaKey chave = new ProdutoLojaKey(produtoLojaDTO.getReferenciaProduto(), produtoLojaDTO.getIdLoja());
                        ProdutoLoja produtoLoja = (ProdutoLoja) em.find(ProdutoLoja.class, chave);

                        produtoLoja.setProdutoReservado(Boolean.TRUE);
                        //persist ?
                        linhasEncomendaFinal.add(new EncomendaLinha(produto, produtoLojaDTO.getNumeroSerie(), null));
                    } else {
                        linhasEncomendaFinal.add(new EncomendaLinha(produto, null, null));
                    }

                    numProdutosEmStockNaLoja--;

                }

                if (numProdutosEmStockNaLoja < 0) {
                    encomenda.getCabecalho().setEstado(Estado.AGUARDA_STOCK);
                }
            }


            CodigoPostal codigoPostal = (CodigoPostal) em.find(CodigoPostal.class, encomenda.getCabecalho().getCodigoPostal());
            if (codigoPostal == null) {
                codigoPostalBean.CriarCodigoPostal(encomenda.getCabecalho().getCodigoPostal(), ((EncomendaCabecalhoConsumidorDTO) encomenda.getCabecalho()).getLocalidade(), lojaDTO.getId());
                codigoPostal = (CodigoPostal) em.find(CodigoPostal.class, encomenda.getCabecalho().getCodigoPostal());
            }


            EncomendaCabecalhoConsumidor encomendaFinal = new EncomendaCabecalhoConsumidor(encomenda.getCabecalho().getEstado(), loja, encomenda.getCabecalho().getDataEncomenda(), ((EncomendaCabecalhoConsumidorDTO) encomenda.getCabecalho()).getNome(), ((EncomendaCabecalhoConsumidorDTO) encomenda.getCabecalho()).getEmailRequerente(), codigoPostal, ((EncomendaCabecalhoConsumidorDTO) encomenda.getCabecalho()).getMorada());

            for (EncomendaLinha encomendaLinha : linhasEncomendaFinal) {
                encomendaLinha.setEncomendaCabecalho(encomendaFinal);
                encomendaFinal.addLinhaEncomenda(encomendaLinha);
            }

            em.persist(encomendaFinal);
            em.flush();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void EnviaEmailConfirmacaoEncomendaAoConsumidor(EncomendaDTO encomenda) throws MessagingException {

        EncomendaCabecalhoConsumidorDTO cabEnc = (EncomendaCabecalhoConsumidorDTO) encomenda.getCabecalho();
        try {
            String corpoEmail;

            corpoEmail = "Caro Sr(a) " + cabEnc.getNome() + " " + cabEnc.getApelido() + "\n\n"
                    + "Confirmamos a sua encomenda com os seguintes dados:\n\n"
                    + "Morada:\t" + cabEnc.getMorada() + "\n"
                    + "Código Postal:\t" + cabEnc.getCodigoPostal() + "\n"
                    + "Localidade:\t" + cabEnc.getLocalidade() + "\n\n"
                    + "Produtos:\n\n";

            for (EncomendaLinhaDTO linhaEnc : encomenda.getLinhas()) {
                corpoEmail = corpoEmail + linhaEnc.getProduto().getNome()
                        + " - "
                        + String.valueOf(linhaEnc.getQuantidade()) + " unidades = "
                        + String.valueOf(linhaEnc.getPrecoTotal()) + " EUR\n\n";
            }

            corpoEmail = corpoEmail + "TOTAL: " + String.valueOf(encomenda.getPrecoTotal()) + " EUR\n\n"
                    + "Obrigado pela preferência!\n\n"
                    + "Com os melhores cumprimentos,\n"
                    + "Loja Virtual";

            emailBean.send(((EncomendaCabecalhoConsumidorDTO) encomenda.getCabecalho()).getEmailRequerente(),
                    "Encomenda confirmada na Loja Virtual", corpoEmail);
        } catch (MessagingException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    public void EnviaEmailEncomendaPagaAoConsumidor(EncomendaCabecalhoConsumidorDTO cabEnc) throws MessagingException {

        //EncomendaCabecalhoConsumidorDTO cabEnc = (EncomendaCabecalhoConsumidorDTO) encomenda.getCabecalho();

        try {
            String corpoEmail;

            corpoEmail = "Caro Sr(a) " + cabEnc.getNome() + " " + cabEnc.getApelido() + "\n\n"
                    + "Confirmamos o pagamento e envio da sua encomenda com os seguintes dados:\n\n"
                    + "Morada:\t" + cabEnc.getMorada() + "\n"
                    + "Código Postal:\t" + cabEnc.getCodigoPostal() + "\n"
                    + "Localidade:\t" + cabEnc.getLocalidade() + "\n\n"
                    + "Produtos:\n\n";
            /*
             for (EncomendaLinhaDTO linhaEnc : encomenda.getLinhas()) {
             corpoEmail = corpoEmail + linhaEnc.getProduto().getNome()
             + " - "
             + String.valueOf(linhaEnc.getQuantidade()) + " unidades = "
             + String.valueOf(linhaEnc.getPrecoTotal()) + " EUR\n\n";
             }
             */
            corpoEmail = corpoEmail //"TOTAL: " + String.valueOf(encomenda.getPrecoTotal()) + " EUR\n\n"
                    + "Obrigado pela preferência!\n\n"
                    + "Com os melhores cumprimentos,\n"
                    + "Loja Virtual";

            emailBean.send(cabEnc.getEmailRequerente(), "Loja Virtual - encomenda paga e enviada", corpoEmail);
        } catch (MessagingException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }
}
