/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import DTO.CategoriaDTO;
import Entidades.Categoria;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author ptumas
 */
@Stateless
public class CategoriaBean {

    @PersistenceContext
    private EntityManager em;

    public void CriarCategoria(int idCategoria, String nome) {

        try {
            Categoria categoria = new Categoria(idCategoria, nome);
            em.persist(categoria);
        } catch (Exception Ex) {
            throw new EJBException(Ex.getMessage());
        }
    }

    public List<CategoriaDTO> getAllCategorias() {

        try {
            List<Categoria> categorias = (List<Categoria>) em.createNamedQuery("findAllCategorias").getResultList();
            return copiarCategoriasParaDTOs(categorias);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    private CategoriaDTO copiarCategoriaParaDTO(Categoria categoria) {
        return new CategoriaDTO(categoria.getId(), categoria.getNome());
    }

    private List<CategoriaDTO> copiarCategoriasParaDTOs(List<Categoria> categorias) {

        List<CategoriaDTO> dtos = new ArrayList<>();

        for (Categoria categoria : categorias) {
            dtos.add(copiarCategoriaParaDTO(categoria));
        }
        return dtos;
    }
}
