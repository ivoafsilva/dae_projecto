/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import DTO.ProdutoDTO;
import EJB.ProdutoBean;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author ptumas
 */
@ManagedBean
@Named(value = "imagensProdutosManager")
@ApplicationScoped
public class ImagensProdutosManager {

    @EJB
    private ProdutoBean produtoBean;
    
    /**
     * Creates a new instance of ImagensProdutosManager
     */
    public ImagensProdutosManager() {
    }
    
    public StreamedContent getImagem() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        }
        else {
            // So, browser is requesting the image. Return a real StreamedContent with the image bytes.
            //String studentId = context.getExternalContext().getRequestParameterMap().get("studentId");
            //Student student = studentService.find(Long.valueOf(studentId));
            //return new DefaultStreamedContent(new ByteArrayInputStream(student.getImage()));
            String referenciaProduto = context.getExternalContext().getRequestParameterMap().get("referencia");
            ProdutoDTO produto = produtoBean.getProduto(referenciaProduto);
            return new DefaultStreamedContent(new ByteArrayInputStream(produto.getImagem()));
        }
    }
}
