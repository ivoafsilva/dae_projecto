/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import DTO.CategoriaDTO;
import DTO.EncomendaCabecalhoConsumidorDTO;
import DTO.EncomendaDTO;
import DTO.EncomendaLinhaDTO;
import DTO.ProdutoDTO;
import EJB.CategoriaBean;
import EJB.EncomendaBean;
import EJB.ProdutoBean;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.faces.event.ActionEvent;
import utils.FacesExceptionHandler;

/**
 *
 * @author ptumas
 */
@Named(value = "consumidorManager")
@SessionScoped
public class ConsumidorManager implements Serializable {

    @EJB
    private CategoriaBean categoriaBean;
    @EJB
    private ProdutoBean produtoBean;
    @EJB
    private EncomendaBean encomendaBean;
    private CategoriaDTO categoriaActual;
    private ProdutoDTO produtoActual;
    private ProdutoDTO produtoARemover;
    private EncomendaDTO carrinhoEncomenda;
    private int qtdAdicionarCarrinho;
    private static final Logger logger = Logger.getLogger("web.AdministradorManager");

    public ConsumidorManager() {
        carrinhoEncomenda = new EncomendaDTO(new EncomendaCabecalhoConsumidorDTO());
        qtdAdicionarCarrinho = 1;
        produtoActual = null;
        produtoARemover = null;
        categoriaActual = null;
    }

    public List<CategoriaDTO> getCategorias() {
        try {
            return categoriaBean.getAllCategorias();
        } catch (Exception e) {
            FacesExceptionHandler.tratarExcecao(e, "Erro do sistema.", logger);
            return null;
        }
    }

    public CategoriaDTO getCategoriaActual() {
        return categoriaActual;
    }

    public void setCategoriaActual(CategoriaDTO categoriaActual) {
        this.categoriaActual = categoriaActual;
    }

    public List<ProdutoDTO> getProdutos() {
        try {
            return produtoBean.getAllProdutos();
        } catch (Exception e) {
            FacesExceptionHandler.tratarExcecao(e, "Erro do sistema.", logger);
            return null;
        }
    }

    public List<ProdutoDTO> getProdutosPorCategoria() {
        try {
            System.out.println("Categoria actual:" + categoriaActual.getNome());
            return produtoBean.getProdutosPorCategoria(categoriaActual.getId());
        } catch (Exception e) {
            FacesExceptionHandler.tratarExcecao(e, "Erro do sistema.", logger);
            return null;
        }
    }

    public String addProdutoCarrinho(ActionEvent actionEvent) {

        boolean produtoJaExisteNoCarrinho = false;
        try {
            if (produtoActual != null) {
                for (EncomendaLinhaDTO encomendaLinhaDTO : carrinhoEncomenda.getLinhas()) {
                    if (encomendaLinhaDTO.getProduto().getReferencia().equals(produtoActual.getReferencia())) {
                        System.out.println("Produto repetido");
                        encomendaLinhaDTO.setQuantidade(qtdAdicionarCarrinho);
                        produtoJaExisteNoCarrinho = true;
                        break;
                    }

                }

                if (!produtoJaExisteNoCarrinho) {
                    carrinhoEncomenda.getLinhas().add(new EncomendaLinhaDTO(produtoActual, qtdAdicionarCarrinho));
                }

                qtdAdicionarCarrinho = 1;
                FacesContext context = FacesContext.getCurrentInstance();
                Flash flash = context.getExternalContext().getFlash();
                flash.setKeepMessages(true);
                flash.setRedirect(true);
                context.addMessage(null, new FacesMessage(produtoActual.getNome() + " adicionado ao carrinho!"));
            } else {
                System.out.println("addProdutoCarrinho - ProdutoActual = NULL!!!");
            }
        } catch (Exception e) {
            FacesExceptionHandler.tratarExcecao(e, "Erro do sistema.", logger);
            return "/index?faces-redirect=true";
        }

        return "/index?faces-redirect=true";
    }

    public String removeProdutoCarrinho() {

        String nomeProdutoRemovido = "";
        String redireccionar = "";

        try {
            if (produtoARemover != null) {
                for (EncomendaLinhaDTO encomendaLinhaDTO : carrinhoEncomenda.getLinhas()) {
                    if (encomendaLinhaDTO.getProduto().getReferencia().equals(produtoARemover.getReferencia())) {
                        System.out.println("Produto a remover");
                        carrinhoEncomenda.getLinhas().remove(encomendaLinhaDTO);
                        nomeProdutoRemovido = produtoARemover.getNome();
                        produtoARemover = null;
                        break;
                    }
                }

                if (carrinhoEncomenda.getLinhas().isEmpty()) {
                    redireccionar = "/index?faces-redirect=true";
                }

                qtdAdicionarCarrinho = 1;
                FacesContext context = FacesContext.getCurrentInstance();
                Flash flash = context.getExternalContext().getFlash();
                flash.setKeepMessages(true);
                flash.setRedirect(true);
                context.addMessage(null, new FacesMessage(nomeProdutoRemovido + " removido do carrinho!"));

            } else {
                System.out.println("addProdutoCarrinho - ProdutoActual = NULL!!!");
            }
        } catch (Exception e) {
            FacesExceptionHandler.tratarExcecao(e, "Erro do sistema.", logger);
        }

        return redireccionar;
    }

    public String submeterEncomenda() {
        FacesContext context = FacesContext.getCurrentInstance();
        Flash flash = context.getExternalContext().getFlash();
        flash.setKeepMessages(true);
        flash.setRedirect(true);

        try {
            carrinhoEncomenda.getCabecalho().setDataEncomenda(new Date());
            encomendaBean.gravarEncomenda(carrinhoEncomenda);
            encomendaBean.EnviaEmailConfirmacaoEncomendaAoConsumidor(carrinhoEncomenda);
            carrinhoEncomenda = new EncomendaDTO(new EncomendaCabecalhoConsumidorDTO());
            context.addMessage(null, new FacesMessage("Encomenda submetida com sucesso!"));
        } catch (Exception e) {
            FacesExceptionHandler.tratarExcecao(e, "Erro do sistema.", logger);
            carrinhoEncomenda = new EncomendaDTO(new EncomendaCabecalhoConsumidorDTO());
            context.addMessage(null, new FacesMessage("Erro ao submeter a encomenda - contacte a loja!"));
            return "/index?faces-redirect=true";
        }

        return "/index?faces-redirect=true";
    }

    public ProdutoDTO getProdutoActual() {
        return produtoActual;
    }

    public void setProdutoActual(ProdutoDTO produtoActual) {
        this.produtoActual = produtoActual;
    }

    public int getQtdAdicionarCarrinho() {
        return qtdAdicionarCarrinho;
    }

    public void setQtdAdicionarCarrinho(int qtdAdicionarCarrinho) {
        this.qtdAdicionarCarrinho = qtdAdicionarCarrinho;
    }

    public ProdutoDTO getProdutoARemover() {
        return produtoARemover;
    }

    public void setProdutoARemover(ProdutoDTO produtoARemover) {
        this.produtoARemover = produtoARemover;
    }

    public EncomendaDTO getCarrinhoEncomenda() {
        return carrinhoEncomenda;
    }

    public void setCarrinhoEncomenda(EncomendaDTO carrinhoEncomenda) {
        this.carrinhoEncomenda = carrinhoEncomenda;
    }
}
