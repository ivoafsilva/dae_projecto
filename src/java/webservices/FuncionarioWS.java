/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webservices;

import DTO.CategoriaDTO;
import DTO.EncomendaCabecalhoConsumidorDTO;
import DTO.FuncionarioDTO;
import DTO.ProdutoDTO;
import EJB.CategoriaBean;
import EJB.EncomendaBean;
import EJB.EncomendaCabecalhoConsumidorBean;
import EJB.FuncionarioBean;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import EJB.ProdutoBean;
import excepcoes.EntidadeNaoExistenteException;
import java.util.List;
import javax.ejb.EJB;
import javax.mail.MessagingException;
import utils.Estado;

/**
 *
 * @author ptumas
 */
@WebService(serviceName = "FuncionarioWS")
@Stateless()
public class FuncionarioWS {

    @EJB
    ProdutoBean produtoBean;
    @EJB
    CategoriaBean categoriaBean;
    @EJB
    EncomendaCabecalhoConsumidorBean encomendaCabecalhoConsumidorBean;
    @EJB
    FuncionarioBean funcionarioBean;
    @EJB
    EncomendaBean encomendaBean;

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    @WebMethod(operationName = "getAllProdutos")
    public List<ProdutoDTO> getAllProdutos() {
        return produtoBean.getAllProdutos();
    }

    @WebMethod(operationName = "getProdutosPorCategoria")
    public List<ProdutoDTO> getProdutosPorCategoria(@WebParam(name = "idCategoria") int idCategoria) throws EntidadeNaoExistenteException {
        return produtoBean.getProdutosPorCategoria(idCategoria);
    }

    @WebMethod(operationName = "getProduto")
    public ProdutoDTO getProduto(@WebParam(name = "referencia") String ref) throws EntidadeNaoExistenteException {
        return produtoBean.getProduto(ref);
    }

    @WebMethod(operationName = "criarProduto")
    public Boolean criarProduto(@WebParam(name = "produto") ProdutoDTO produto) throws EntidadeNaoExistenteException {
        try {
            return produtoBean.criarProduto(produto);
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod(operationName = "alterarProduto")
    public Boolean alterarProduto(@WebParam(name = "produto") ProdutoDTO produto) throws EntidadeNaoExistenteException {
        try {
            return produtoBean.alterarProduto(produto);
        } catch (Exception e) {
            return false;
        }
    }

    @WebMethod(operationName = "getAllCategorias")
    public List<CategoriaDTO> getAllCategorias() {
        return categoriaBean.getAllCategorias();
    }

    @WebMethod(operationName = "getAllEncomendasDespachadasConsumidor")
    public List<EncomendaCabecalhoConsumidorDTO> getAllEncomendasDespachadasConsumidor(int id_loja) {
        return encomendaCabecalhoConsumidorBean.getAllEncomendasConsumidorPorEstado(id_loja, Estado.DESPACHADA);
    }

    @WebMethod(operationName = "getAllEncomendasNaoDespachadasConsumidor")
    public List<EncomendaCabecalhoConsumidorDTO> getAllEncomendasNaoDespachadasConsumidor(int id_loja) {
        return encomendaCabecalhoConsumidorBean.getAllEncomendasConsumidorPorEstado(id_loja, Estado.NAO_DESPACHADA);
    }

    @WebMethod(operationName = "getAllEncomendasAguardarStockConsumidor")
    public List<EncomendaCabecalhoConsumidorDTO> getAllEncomendasAguardarStockConsumidor(int id_loja) {
        return encomendaCabecalhoConsumidorBean.getAllEncomendasConsumidorPorEstado(id_loja, Estado.AGUARDA_STOCK);
    }

    @WebMethod(operationName = "getAllFuncionarios")
    public List<FuncionarioDTO> getAllFuncionarios() {
        return funcionarioBean.getAllFuncionarios();
    }

    @WebMethod(operationName = "despacharEncomenda")
    public void despacharEncomenda(EncomendaCabecalhoConsumidorDTO encomenda) throws MessagingException {
       encomendaBean.EnviaEmailEncomendaPagaAoConsumidor(encomenda);
    }
    
}
