/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

public enum Estado {

    DESPACHADA("DESPACHADA"),
    NAO_DESPACHADA("NAO_DESPACHADA"),
    AGUARDA_STOCK("AGUARDA_STOCK");
    
    private String descricao;

    Estado(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return this.descricao;
    }

}

/*
 *  public static Estado parse(String descricao) {
        Estado estado = null;
        for (Estado item : Estado.values()) {
            if (item.getDescricao().equals(descricao)) {
                estado = item;
                break;
            }
        }
        return estado;
    }
 * 
 * 
   public Estado getEstado () {
        return Estado.parse(this.estado);
    }
 
     public void setEstado(Estado estado) {
        this.estado = estado.getDescricao();
    }
 */