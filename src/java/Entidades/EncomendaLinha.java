/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class EncomendaLinha implements Serializable {

    @Id
    @NotNull
    private String id;
    @NotNull
    @ManyToOne
    private Produto produto;
    @NotNull
    @ManyToOne
    private EncomendaCabecalho encomendaCabecalho;
    //@NotNull
    //private int quantidade;
    //@NotNull
    private String numeroSerie;

    public EncomendaLinha(Produto produto, String numeroSerie, EncomendaCabecalho encomendaCabecalho) {
        this.id = UUID.randomUUID().toString();
        this.produto = produto;
        this.numeroSerie = numeroSerie;
        this.encomendaCabecalho = encomendaCabecalho;
    }

    public EncomendaLinha() {
    }

    public String getId() {
        return id;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public EncomendaCabecalho getEncomendaCabecalho() {
        return encomendaCabecalho;
    }

    public void setEncomendaCabecalho(EncomendaCabecalho encomendaCabecalho) {
        this.encomendaCabecalho = encomendaCabecalho;
    }
}
