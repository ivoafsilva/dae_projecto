/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import utils.Estado;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
    @NamedQuery(name = "findAllEncomendasFuncionario",
            query = "SELECT e FROM EncomendaCabecalhoFuncionario e ORDER BY e.dataEncomenda"),
    @NamedQuery(name = "findEncomendasDespachadasFuncionario",
            query = "SELECT e FROM EncomendaCabecalhoFuncionario e WHERE e.estado = 0 ORDER BY e.dataEncomenda"),
    @NamedQuery(name = "findEncomendasNaoDespachadasFuncionario",
            query = "SELECT e FROM EncomendaCabecalhoFuncionario e WHERE e.estado = 1 ORDER BY e.dataEncomenda"),
    @NamedQuery(name = "findEncomendasAguardarStockFuncionario",
            query = "SELECT e FROM EncomendaCabecalhoFuncionario e WHERE e.estado = 2 ORDER BY e.dataEncomenda"),
    @NamedQuery(name = "findEncomendasFuncionarioPorLoja",
            query = "SELECT e FROM EncomendaCabecalhoFuncionario e WHERE e.lojaExpedicao = :idLoja ORDER BY e.dataEncomenda")})
public class EncomendaCabecalhoFuncionario extends EncomendaCabecalho implements Serializable {

    private Funcionario funcionarioAEncomendar;

    public EncomendaCabecalhoFuncionario(Estado estado, Loja loja, Date data, CodigoPostal codigoPostal, Funcionario funcionarioAEncomendar) {
        super(estado, loja, data, codigoPostal);
        this.funcionarioAEncomendar = funcionarioAEncomendar;
    }

    public EncomendaCabecalhoFuncionario() {
    }

    public Funcionario getFuncionarioAEncomendar() {
        return funcionarioAEncomendar;
    }

    public void setFuncionarioAEncomendar(Funcionario funcionarioAEncomendar) {
        this.funcionarioAEncomendar = funcionarioAEncomendar;
    }
}
