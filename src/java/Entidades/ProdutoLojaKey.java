/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.Objects;



public class ProdutoLojaKey implements Serializable {

    private String produto;
    private int loja;

    public ProdutoLojaKey() {
    }

    
    
    public ProdutoLojaKey(String produto, int loja) {
        this.produto = produto;
        this.loja = loja;
    }

    /**
     * @return the produto
     */
    public String getProduto() {
        return produto;
    }

    /**
     * @param produto the produto to set
     */
    public void setProduto(String produto) {
        this.produto = produto;
    }

    /**
     * @return the loja
     */
    public int getLoja() {
        return loja;
    }

    /**
     * @param loja the loja to set
     */
    public void setLoja(int loja) {
        this.loja = loja;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.produto);
        hash = 47 * hash + this.loja;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProdutoLojaKey other = (ProdutoLojaKey) obj;
        if (!Objects.equals(this.produto, other.produto)) {
            return false;
        }
        if (this.loja != other.loja) {
            return false;
        }
        return true;
    }

    
    
    
    
}
