/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Loja implements Serializable {

    @Id
    private int idLoja;
    @OneToMany(mappedBy = "loja")
    private List<ProdutoLoja> produtosLoja;
    @OneToMany(mappedBy = "loja")
    private List<Funcionario> funcionarios;
    @OneToMany(mappedBy = "lojaExpedicao")
    private List<EncomendaCabecalho> encomendas;
    //Localidades servidas por esta Loja
    @OneToMany(mappedBy = "loja")
    private List<CodigoPostal> codigosPostais;
    private String nome;
    

    public Loja(int id_loja, String nome) {
        this.idLoja = id_loja;
        this.nome = nome;
        this.codigosPostais = new ArrayList<>();
        this.produtosLoja = new ArrayList<>();
        this.funcionarios = new ArrayList<>();
        this.encomendas = new ArrayList<>();
    }

    public Loja() {
        this.produtosLoja = new ArrayList<>();
        this.funcionarios = new ArrayList<>();
        this.encomendas = new ArrayList<>();
    }

    public List<Funcionario> getFuncionarios() {
        return this.funcionarios;
    }

    public void addFuncionario(Funcionario funcionario) {
        this.funcionarios.add(funcionario);
    }

    public void removeFuncionario(Funcionario funcionario) {
        this.funcionarios.remove(funcionario);
    }

    public List<ProdutoLoja> getProdutosLoja() {
        return produtosLoja;
    }

    public void addProdutosLoja(ProdutoLoja produtoLoja) {
        this.produtosLoja.add(produtoLoja);
    }

    public void removeProdutosLoja(ProdutoLoja produtoLoja) {
        this.produtosLoja.add(produtoLoja);
    }

    public List<EncomendaCabecalho> getEncomendaCabecalhos() {
        return encomendas;
    }

    public void addEncomenda(EncomendaCabecalho encomenda) {
        this.encomendas.add(encomenda);
    }

    public void removeEncomenda(EncomendaCabecalho encomenda) {
        this.encomendas.remove(encomenda);
    }

    /**
     * @return the id
     */
    public int getIdLoja() {
        return idLoja;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void addCodigoPostal(CodigoPostal codigoPostal) {
        this.codigosPostais.add(codigoPostal);
    }

    public void removeCodigoPostal(CodigoPostal codigoPostal) {
        this.codigosPostais.remove(codigoPostal);
    }
}
