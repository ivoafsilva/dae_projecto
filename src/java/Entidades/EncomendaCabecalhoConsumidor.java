/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import utils.Estado;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


@Entity
@NamedQueries({
    @NamedQuery(name = "findAllEncomendasConsumidor",
    query = "SELECT e FROM EncomendaCabecalhoConsumidor e ORDER BY e.dataEncomenda"),
    @NamedQuery(name = "findEncomendasDespachadasConsumidor",
    query = "SELECT e FROM EncomendaCabecalhoConsumidor e WHERE e.estado = 0 ORDER BY e.dataEncomenda"),
    @NamedQuery(name = "findEncomendasNaoDespachadasConsumidor",
    query = "SELECT e FROM EncomendaCabecalhoConsumidor e WHERE e.estado = 1 ORDER BY e.dataEncomenda"),
    @NamedQuery(name = "findEncomendasAguardarStockConsumidor",
    query = "SELECT e FROM EncomendaCabecalhoConsumidor e WHERE e.estado = 2 ORDER BY e.dataEncomenda"),
    @NamedQuery(name = "findEncomendasConsumidorPorLoja",
            query = "SELECT e FROM EncomendaCabecalhoConsumidor e WHERE e.lojaExpedicao = :idLoja ORDER BY e.dataEncomenda")})
public class EncomendaCabecalhoConsumidor extends EncomendaCabecalho implements Serializable {

    @NotNull
    private String nomeRequerente;
    @NotNull
    @Pattern(regexp = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
            + "[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
            + "(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
            message = "{invalid.email}")
    private String emailRequerente;
    
    private String morada;
    
    public EncomendaCabecalhoConsumidor() {
    }

    public EncomendaCabecalhoConsumidor(Estado estado, Loja loja, Date data, String nomeRequerente, String emailRequerente, CodigoPostal codigoPostal, String morada) {
        super(estado, loja, data, codigoPostal);
        this.morada = morada;
        this.nomeRequerente = nomeRequerente;
        this.emailRequerente = emailRequerente;
    }

    public String getMorada() {
        return morada;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }
    
    public String getNomeRequerente() {
        return nomeRequerente;
    }

    public void setNomeRequerente(String nomeRequerente) {
        this.nomeRequerente = nomeRequerente;
    }

    public String getEmailRequerente() {
        return emailRequerente;
    }

    public void setEmailRequerente(String emailRequerente) {
        this.emailRequerente = emailRequerente;
    }
}
