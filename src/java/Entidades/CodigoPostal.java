/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@NamedQueries({
    @NamedQuery(name = "findCodigoPostal",
            query = "SELECT c FROM CodigoPostal c WHERE c.codigoPostal = :codigoPostal")})
public class CodigoPostal implements Serializable {

    @Id
    @Pattern(regexp = "[0-9]{4}\\-[0-9]{3}", message = "{invalid.codigopostal}")
    private String codigoPostal;
    @NotNull
    private String localidade;
    @NotNull
    @ManyToOne
    private Loja loja;
    @OneToMany(mappedBy = "codigoPostal")
    private List<EncomendaCabecalho> encomendasCabecalho;

    public CodigoPostal() {
    }

    public CodigoPostal(String codigo_postal, String localidade, Loja loja) {
        this.codigoPostal = codigo_postal;
        this.localidade = localidade;
        this.loja = loja;
        this.encomendasCabecalho = new ArrayList<>();
    }

    public String getCodigo_postal() {
        return codigoPostal;
    }

    public void setCodigo_postal(String codigo_postal) {
        this.codigoPostal = codigo_postal;
    }

    public String getLocalidade() {
        return localidade;
    }

    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public List<EncomendaCabecalho> getEncomendasCabecalho() {
        return encomendasCabecalho;
    }

    public void addEncomenda(EncomendaCabecalho encomenda) {
        this.encomendasCabecalho.add(encomenda);
    }

    public void removeEncomenda(EncomendaCabecalho encomenda) {
        this.encomendasCabecalho.remove(encomenda);
    }
}
