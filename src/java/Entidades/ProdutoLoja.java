/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@Entity
@IdClass(ProdutoLojaKey.class)
@Table(name = "PRODUTOLOJA",
        uniqueConstraints =
        @UniqueConstraint(columnNames = {"NUMEROSERIE"}))
@NamedQueries({
    @NamedQuery(name = "findNumerosSerieReservados",
            query = "SELECT p FROM ProdutoLoja p WHERE p.produto.referencia = :referenciaProduto AND p.loja.idLoja = :idLoja AND p.produtoReservado = 1"),
    @NamedQuery(name = "findNumerosSerieNaoReservados",
            query = "SELECT p FROM ProdutoLoja p WHERE p.produto.referencia = :referenciaProduto AND p.loja.idLoja = :idLoja AND p.produtoReservado = 0"),
    @NamedQuery(name = "countNumerosSerieReservados",
            query = "SELECT COUNT(p) FROM ProdutoLoja p WHERE p.produto.referencia = :referenciaProduto AND p.loja.idLoja = :idLoja AND p.produtoReservado = 1"),
    @NamedQuery(name = "countNumerosSerieNaoReservados",
            query = "SELECT COUNT(p) FROM ProdutoLoja p WHERE p.produto.referencia = :referenciaProduto AND p.loja.idLoja = :idLoja AND p.produtoReservado = 0")
})
public class ProdutoLoja implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "REFERENCIA_PRODUTO")
    private Produto produto;
    @Id
    @ManyToOne
    @JoinColumn(name = "ID_LOJA")
    private Loja loja;
    @NotNull
    private String numeroSerie;
    private Boolean produtoReservado;

    public ProdutoLoja(Produto produto, Loja loja, String numeroSerie) {
        this.produto = produto;
        this.loja = loja;
        this.numeroSerie = numeroSerie;
        this.produtoReservado = false;
    }

    public ProdutoLoja() {
    }

    /**
     * @return the produto
     */
    public Produto getProduto() {
        return produto;
    }

    /**
     * @param produto the produto to set
     */
    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    /**
     * @return the loja
     */
    public Loja getLoja() {
        return loja;
    }

    /**
     * @param loja the loja to set
     */
    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public Boolean isProdutoReservado() {
        return produtoReservado;
    }

    public void setProdutoReservado(Boolean produtoReservado) {
        this.produtoReservado = produtoReservado;
    }
}
