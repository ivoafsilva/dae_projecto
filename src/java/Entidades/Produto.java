/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
@NamedQuery(name = "findAllProdutos",
        query = "SELECT p FROM Produto p ORDER BY p.referencia")
public class Produto implements Serializable {

    @Id
    @NotNull
    private String referencia;
    @NotNull
    private String nome;
    private String descricao;
    private String descricaoLonga;
    @Lob
    private byte[] imagem;
    @NotNull
    private float preco;
    @OneToMany(mappedBy = "produto")
    private List<EncomendaLinha> linhasEncomendas;
    @OneToMany(mappedBy = "produto")
    private List<ProdutoLoja> produtosLoja;
    @ManyToOne
    private Categoria categoria;

    public Produto(String referencia, String nome, String descricao, String descricaoLonga, byte[] imagem, float preco, Categoria categoria) {
        this.referencia = referencia;
        this.nome = nome;
        this.descricao = descricao;
        this.descricaoLonga = descricaoLonga;
        this.imagem = imagem;
        this.preco = preco;
        this.categoria = categoria;
        this.linhasEncomendas = new ArrayList<>();
        this.produtosLoja = new ArrayList<>();
    }

    public Produto() {
        this.linhasEncomendas = new ArrayList<>();
        this.produtosLoja = new ArrayList<>();
    }

    public List<EncomendaLinha> getLinhasEncomendas() {
        return linhasEncomendas;
    }

    public void addLinhaEncomendas(EncomendaLinha linhaEncomenda) {
        this.linhasEncomendas.add(linhaEncomenda);
    }

    public void removeLinhaEncomendas(EncomendaLinha linhaEncomenda) {
        this.linhasEncomendas.remove(linhaEncomenda);
    }

    public List<ProdutoLoja> getProdutosLoja() {
        return produtosLoja;
    }

    public void addProdutosLoja(ProdutoLoja produtoLoja) {
        this.produtosLoja.add(produtoLoja);
    }

    public void removeProdutosLoja(ProdutoLoja produtoLoja) {
        this.produtosLoja.remove(produtoLoja);
    }

    /**
     * @return the referencia
     */
    public String getReferencia() {
        return referencia;
    }

    /**
     * @param referencia the referencia to set
     */
    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the imagem
     */
    public byte[] getImagem() {
        return imagem;
    }

    /**
     * @param imagem the imagem to set
     */
    public void setImagem(byte[] imagem) {
        this.imagem = imagem;
    }

    /**
     * @return the preco
     */
    public float getPreco() {
        return preco;
    }

    /**
     * @param preco the preco to set
     */
    public void setPreco(float preco) {
        this.preco = preco;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    /**
     * @return the descricaoLonga
     */
    public String getDescricaoLonga() {
        return descricaoLonga;
    }

    /**
     * @param descricaoLonga the descricaoLonga to set
     */
    public void setDescricaoLonga(String descricaoLonga) {
        this.descricaoLonga = descricaoLonga;
    }
}
