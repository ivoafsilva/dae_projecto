/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import utils.Estado;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;


@Entity

public abstract class EncomendaCabecalho implements Serializable {
 
    @Id
    private String idEncomenda;
    
    @NotNull
    private Estado estado;
    
    @NotNull
    @ManyToOne
    private Loja lojaExpedicao;
    
    //@NotNull
    @ManyToOne
    private Funcionario funcionarioExpedicao;
    
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataEncomenda;
    
    @OneToMany(mappedBy = "encomendaCabecalho", cascade = CascadeType.PERSIST)
    private List<EncomendaLinha> linhasEncomenda;
    
    @ManyToOne
    private CodigoPostal codigoPostal;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataExpedicao;
    

    public EncomendaCabecalho(Estado estado, Loja lojaExpedicao, Date dataEncomenda, CodigoPostal codigoPostal) {
        this.idEncomenda = UUID.randomUUID().toString();
        this.estado = estado;
        this.dataEncomenda = dataEncomenda;
        this.lojaExpedicao = lojaExpedicao;
        this.codigoPostal = codigoPostal;
        this.dataExpedicao = null;
        this.funcionarioExpedicao = null;
        this.linhasEncomenda = new ArrayList<>();        
    }

    public EncomendaCabecalho() {
        this.linhasEncomenda = new ArrayList<>();
    }
    
    
    public String getIdEncomenda() {
        return idEncomenda;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Loja getLojaExpedicao() {
        return lojaExpedicao;
    }

    public void setLojaExpedicao(Loja loja) {
        this.lojaExpedicao = loja;
    }

    public Date getDataEncomenda() {
        return dataEncomenda;
    }

    public void setDataEncomenda(Date dataEncomenda) {
        this.dataEncomenda = dataEncomenda;
    }

    public List<EncomendaLinha> getLinhasEncomenda() {
        return linhasEncomenda;
    }

    public void addLinhaEncomenda(EncomendaLinha linha) {
        this.linhasEncomenda.add(linha);
    }
    
    public void removeLinhaEncomenda(EncomendaLinha linha) {
        this.linhasEncomenda.remove(linha);
    }

    public Funcionario getFuncionarioExpedicao() {
        return funcionarioExpedicao;
    }

    public void setFuncionarioExpedicao(Funcionario funcionario) {
        this.funcionarioExpedicao = funcionario;
    }

    public CodigoPostal getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(CodigoPostal codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public Date getDataExpedicao() {
        return dataExpedicao;
    }

    public void setDataExpedicao(Date dataExpedicao) {
        this.dataExpedicao = dataExpedicao;
    }
    
    
    
     
}
