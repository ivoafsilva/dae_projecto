/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@NamedQuery(name = "findAllFuncionarios",
        query = "SELECT f FROM Funcionario f")
public class Funcionario implements Serializable {

    @NotNull
    private String nome;
    @Id
    @NotNull
    @Pattern(regexp = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
            + "[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
            + "(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
            message = "{invalid.email}")
    private String email;
    @NotNull
    @ManyToOne
    private Loja loja;
    @OneToMany(mappedBy = "funcionarioExpedicao")
    private List<EncomendaCabecalho> encomendas;
    @NotNull
    private char[] password;

    public Funcionario() {
    }

    public Funcionario(String nome, String email, char[] password, Loja loja) {
        this.nome = nome;
        this.email = email;
        this.loja = loja;
        this.password = hashPassword(password);
        this.encomendas = new ArrayList<>();
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<EncomendaCabecalho> getEncomendas() {
        return encomendas;
    }

    public void addEncomenda(EncomendaCabecalho encomendaCabecalho) {
        this.encomendas.add(encomendaCabecalho);
    }

    public void removeEncomenda(EncomendaCabecalho encomendaCabecalho) {
        this.encomendas.remove(encomendaCabecalho);
    }

    public void setPassword(char[] password) {
        this.password = hashPassword(password);
    }

    public char[] getPassword() {
        return password;
    }

    private char[] hashPassword(char[] password) {
        char[] encoded = null;
        try {
            ByteBuffer passwdBuffer = Charset.defaultCharset().encode(CharBuffer.wrap(password));
            byte[] passwdBytes = passwdBuffer.array();
            MessageDigest mdEnc = MessageDigest.getInstance("SHA-256");
            mdEnc.update(passwdBytes, 0, password.length);
            encoded = new BigInteger(1, mdEnc.digest()).toString(16).toCharArray();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Funcionario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return encoded;
    }
}
